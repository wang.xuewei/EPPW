﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.SampleMng.Dao;
using EPPW.Collections;

namespace EPPW.SampleMng.Service
{
    public class SampleService:ISampleService
    {
        public ISampleDao SampleDao { private get; set; }
        public DateTime GetDate()
        {
            return SampleDao.GetDate();
        }
        public void GetSqlException()
        {
             SampleDao.GetSqlException();
        }

    }
}
