﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Collections;

namespace EPPW.SampleMng.Dao
{
    public interface ISampleDao 
    {
        DateTime GetDate();
        void GetSqlException();

    }
}
