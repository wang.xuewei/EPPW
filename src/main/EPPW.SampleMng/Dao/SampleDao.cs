﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Data.NHibernate.Generic.Support;
using EPPW.Collections;

namespace EPPW.SampleMng.Dao
{
    public class SampleDao : HibernateDaoSupport, ISampleDao
    {
        public DateTime GetDate()
        {
            return Session.GetNamedQuery("SampleDao_GetDate")
                     .UniqueResult<DateTime>();
        }
        public void GetSqlException()
        {
            Session.GetNamedQuery("SampleDao_TestException")
                    .List();
        }

       
    }
}
