﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sliverant.Core.Utils;
using System.Web;

namespace EPPW.Web.Common.Helper
{
    public class CultureHelper
    {

        private static string GetBrowseLang()
        {
            string[] langs = HttpContext.Current.Request.UserLanguages;
            string firstLang = string.Empty;
            if (langs.Length > 0)
            {
                firstLang = langs[0];
                switch (langs[0])
                {
                    case "zh":
                        firstLang = "zh-CN";
                        break;
                    case "en":
                        firstLang = "en-US";
                        break;
                    case "ko":
                        firstLang = "ko-KR";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                firstLang = "en-US";
            }

            return firstLang;
        }

        public static void InitCulture(ICultureHelper CultureService)
        {
            string cultureName = GetBrowseLang();
            CultureService.CurrentCultureName = cultureName;
        }
    }
}
