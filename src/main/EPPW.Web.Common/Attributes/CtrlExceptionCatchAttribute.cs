﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;

namespace EPPW.Web.Common.Attributes
{
    public class CtrlExceptionCatchAttribute : HandleErrorAttribute
    {
        //public override void OnException(ExceptionContext filterContext)
        //{
        //    base.OnException(filterContext);
        //    if (filterContext.HttpContext.Request.IsAjaxRequest())
        //    {
        //        filterContext.Result = new JsonResult
        //        {
        //            Data = new
        //            {
        //                result = "Failure",
        //                msg = filterContext.Exception.Message,
        //                data = new { }
        //            },
        //            ContentType = null,
        //            ContentEncoding = null,
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet
        //        };
        //    }
        //    else
        //    {
        //        filterContext.Result = new RedirectResult(FormsAuthentication.LoginUrl);
        //    }
        //    filterContext.ExceptionHandled = true;
        //}
        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        result = "Failure",
                        msg = filterContext.Exception.Message,
                        data = new { }
                    },
                    ContentType = null,
                    ContentEncoding = null,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
            }
            filterContext.ExceptionHandled = true;
        }
    }
}
