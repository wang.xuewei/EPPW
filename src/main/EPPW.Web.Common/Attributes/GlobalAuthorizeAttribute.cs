﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Security;
using System.Web;
using System.Web.Script.Serialization;
using EPPW.Membership.Service;
using EPPW.Membership.Dto;
using EPPW.Web.Common.Models;
using Sliverant.Core.Context;
using Sliverant.Core.Utils;
using Sliverant.Web.Common.Models;
using Sliverant.Web.Common.Extends;
using Sliverant.Web.Common.Utils;

namespace EPPW.Web.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class GlobalAuthorizeAttribute : AuthorizeAttribute
    {

        public string[] IgnoreControllerNames { get; set; }

        public virtual IRoleService RoleService { get { return SpringContext.GetObject<IRoleService>("RoleService"); } }
        public virtual IUserService UserService { get { return SpringContext.GetObject<IUserService>("UserService"); } }

        public ISessionHelper SessionService { get { return SpringContext.GetObject<ISessionHelper>("SessionService"); } }
        public IResourceHelper ResourceService { get { return SpringContext.GetObject<IResourceHelper>("ResourceService"); } }
        public ICultureHelper CultureService { get { return SpringContext.GetObject<ICultureHelper>("CultureService"); } }

        //IUserAccountProvider
        public GlobalAuthorizeAttribute() { }

        public void CacheValidationHandler(HttpContext context,
                                    object data,
                                    ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }
        /// <summary>
        /// 验证用户是否登录
        /// 用户是否拥有访问页面的权限
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //Modify By JiangYaFei 2014-7-30 09:11:38
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            //End Modify

            string controllerName = filterContext.RouteData.Values["controller"] as string;
            string actionName = filterContext.RouteData.Values["action"] as string;

            // 匿名用户不用验证
            bool skipAuthentication = filterContext.ActionDescriptor
                .IsDefined(typeof(AllowAnonymousAttribute), true)
                || filterContext.ActionDescriptor.ControllerDescriptor
                .IsDefined(typeof(AllowAnonymousAttribute), true);
            if (skipAuthentication)
            {
                return;
            }
         
            // 验证是否登录
            if (AuthorizeCore(filterContext.HttpContext))
            {
                // ** IMPORTANT **
                // Since we're performing authorization at the action level, the authorization code runs
                // after the output caching module. In the worst case this could allow an authorized user
                // to cause the page to be cached, then an unauthorized user would later be served the
                // cached page. We work around this by telling proxies not to cache the sensitive page,
                // then we hook our custom authorization code into the caching mechanism so that we have
                // the final say on whether a page should be served from the cache.
                //大意是：我们把验证放到了Action部分，使得验证代码将在缓存模块后面执行。
                //考虑到最坏的情况，一个认证的用户得到了一个敏感的页面，
                //并且这个页面被缓存了，接着，一个未验证的用户将得到缓存页面而不需验证。
                //我们绕过了这个问题，直接告诉代理不要缓存敏感页面，
                //并且把我们的验证机制注入到缓存机制中，使我们最终决定是否返回缓存页面
                HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
                cachePolicy.SetProxyMaxAge(new TimeSpan(0));
                cachePolicy.AddValidationCallback(this.CacheValidationHandler, null /* data */);
            }
            else
            {
                HandleUnauthorizedRequest(filterContext);
                return;
            }
            bool skipAuthorization = filterContext.ActionDescriptor
                .IsDefined(typeof(AllowEveryoneAttribute), true)
                || filterContext.ActionDescriptor.ControllerDescriptor
                .IsDefined(typeof(AllowEveryoneAttribute), true);
            if (skipAuthorization)
            {
                return;
            }

      

            Dictionary<string, object> values = new Dictionary<string, object>();
            foreach (KeyValuePair<string, object> v in filterContext.RequestContext.RouteData.Values)
                values.Add(v.Key, v.Value);

            // check from provided program list
            if (this.HasAccessRight(SessionService.UserInfo.UserId,
                 values))
            {
                return;
            }
            else
            {
                filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
            }

            // 对页面没有访问权限
            this.HandleUnauthorizedRequest(filterContext);
        }

        private bool HasAccessRight(int userId, Dictionary<string, object> routeData)
        {
            string controllerName = (string)routeData["controller"];
            string actionName = (string)routeData["action"];

            string programSessionKey = SessionKeys.SESSION_AUTH_PROGRAM;

            List<ProgramDto> programDtoList = null;
            programDtoList = SessionService.GetValue(programSessionKey) as List<ProgramDto>;

            if (programDtoList == null)
            {
                programDtoList = RoleService.GetAllActionOfHasAccessRightUser(userId);
                SessionService.SetValue(programSessionKey, programDtoList);
            }

            if (programDtoList != null && programDtoList.Count > 0)
            {
                var programDto = from p in programDtoList
                                 where p.Action.ToLower() == actionName.ToLower() && p.Controller.ToLower() == controllerName.ToLower()
                                 select p;
                if (programDto != null && programDto.Count() > 0)
                {
                    return true;
                }
            }
            return false;
        
        }

        /// <summary>
        /// 验证是否登录
        /// 如果登录，设置Session
        /// </summary>
        /// <param name="httpContext">httpContext</param>
        /// <returns>是否认证</returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            string userName = null;
            bool succeed = base.AuthorizeCore(httpContext);
            if (succeed && httpContext.User.Identity.IsAuthenticated)
                userName = httpContext.User.Identity.Name;

            //session过期了 但是凭证还在 delete by xiao.xinmiao 2015-12-29 06:37:26
            if (succeed && SessionService.UserInfo.UserId== 0)
            {
                UserDto userDto = this.UserService.GetUserByUserName(userName);
                if (userDto != null)
                {

                    // forms authentication compatability
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(userDto);
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                        1,
                        userDto.UserName,
                        DateTime.Now,
                        DateTime.Now.AddMinutes(30),
                        false,
                        userData);
                    string enc = FormsAuthentication.Encrypt(ticket);
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, enc);
                    httpContext.Response.Cookies.Add(cookie);
                    // without reload, apply authentication
                    SliverantPricipal principal = new SliverantPricipal(userDto.UserName);
                    HttpContext.Current.User = principal;
                    SessionService.UserInfo = userDto;
                }
            }
            //如果session不存在了，则权限验证失败
            return succeed && SessionService.UserInfo.UserId != 0;
        }

        #region HandleUnauthorizedRequest
        /// <summary>
        /// 没有权限时的处理方式
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            IPrincipal user = filterContext.HttpContext.User;
            string message = null;
            int statusCode = 400;
            if (!user.Identity.IsAuthenticated)
            {
                statusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                message = ResourceService.GetMessage("SA001");
                //"Authentication credentials were missing or incorrect.";
            }
            else
            {
                statusCode = (int)System.Net.HttpStatusCode.Forbidden;
                message = ResourceService.GetMessage("SA002");
                //"To access this page, proper access rights must be required.";
            }
            filterContext.HttpContext.Response.StatusCode = statusCode;

            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                if (statusCode == 401)//为了避免服务端401直接重定向到login页面，故做此判断
                {
                    filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Accepted;
                }
                filterContext.Result = new JsonResult()
                {
                    Data = new
                    {
                        result = "Failure",
                        msg = message,
                        realStatus = statusCode,
                        returnurl = FormsAuthentication.LoginUrl
                    },
                    ContentType = null,
                    ContentEncoding = null,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                string url = filterContext.HttpContext.Server.UrlEncode(filterContext.HttpContext.Request.Url.OriginalString);
                switch (statusCode)
                {
                    case 401:
                        filterContext.Result = new RedirectResult(FormsAuthentication.LoginUrl + "?returnUrl=" + url);
                        break;
                    case 403:
                        filterContext.Result = new RedirectResult("/Error/Http403");
                        break;
                    default:
                        break;
                }
                //filterContext.Result = statusCode == 401
                //    ? new RedirectResult(FormsAuthentication.LoginUrl + "?returnUrl=" + url)
                //    : new RedirectResult("/Error/Http403");
            }
        }

        #endregion

    }
}
