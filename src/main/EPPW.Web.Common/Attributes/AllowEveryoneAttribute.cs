﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPPW.Web.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class AllowEveryoneAttribute : Attribute
    {
    }
}
