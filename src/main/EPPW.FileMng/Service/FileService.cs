﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Collections;
using EPPW.FileMng.Dao;
using EPPW.FileMng.Dto;

namespace EPPW.FileMng.Service
{
    public class FileService : IFileService
    {
        public IFileDao FileDao { private get; set; }
        public FileDto GetFileByFileID(NameValueList args)
        {
            return FileDao.GetFileByFileID(args);

        }
        public FileDto SaveFile(NameValueList args)
        {
            return FileDao.SaveFile(args);
        }
    }
}

