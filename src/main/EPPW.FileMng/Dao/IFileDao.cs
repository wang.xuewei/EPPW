﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Collections;
using EPPW.FileMng.Dto;

namespace EPPW.FileMng.Dao
{
    public interface IFileDao 
    {
        FileDto GetFileByFileID(NameValueList args);
        FileDto SaveFile(NameValueList args);
    }
}
