﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Data.NHibernate.Generic.Support;
using EPPW.Collections;
using NHibernate.Transform;
using EPPW.FileMng.Dto;

namespace EPPW.FileMng.Dao
{
    public class FileDao : HibernateDaoSupport, IFileDao
    {
        public FileDto GetFileByFileID(NameValueList args)
        {
            int fileID = (int)args["FileID"];
            return Session.GetNamedQuery("FileDao_GetFileByFileID")
              .SetResultTransformer(Transformers.AliasToBean(typeof(FileDto)))
              .SetInt32("FileID", fileID)
              .UniqueResult<FileDto>() as FileDto;

        }
        public FileDto SaveFile(NameValueList args)
        {

            FileDto fileDto = args["FileDto"] as FileDto;
            return Session.GetNamedQuery("FileDao_SaveFile")
                .SetResultTransformer(Transformers.AliasToBean(typeof(FileDto)))
                .SetString("FileName", fileDto.FileName)
                .SetString("FileExtName", fileDto.FileExtName)
                //222
                .SetString("FilePath", fileDto.FilePath)
                .SetInt32("InUserID", fileDto.InUserID)
                .UniqueResult<FileDto>() as FileDto;
             //111
            //444
            //5555
            //6666
            //77777
            //88

       }
        public FileDto SaveFile3(NameValueList args)
        {
            //11111
            FileDto fileDto = args["FileDto"] as FileDto;
            return Session.GetNamedQuery("FileDao_SaveFile")
                .SetResultTransformer(Transformers.AliasToBean(typeof(FileDto)))
                .SetString("FileName", fileDto.FileName)
                .SetString("FileExtName", fileDto.FileExtName)
                .SetString("FilePath", fileDto.FilePath)
                .SetInt32("InUserID", fileDto.InUserID)
                .UniqueResult<FileDto>() as FileDto;
       }
    }
}
