﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using EPPW.Web.Common.Attributes;
using System.Web.Optimization;
using Sliverant.Web.Common.Utils;
using Sliverant.Web.Common.Attributes;

namespace EPPW.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : SliverAntMvcApplication
    {
        public override void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            base.RegisterGlobalFilters(filters);
            filters.Add(new GlobalAuthorizeAttribute());
            //filters.Add(new GZipAttribute());
            filters.Add(new CtrlExceptionCatchAttribute());
        }

        protected override void RegisterMvcRoute(RouteCollection routes)
        {
            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "HomePage", id = UrlParameter.Optional } // Parameter defaults
                , new string[] { "EPPW.Web.Controllers" }
                );

            //routes.MapRoute(
            //    "Default", // Route name
            //    "{controller}/{action}/{id}", // URL with parameters
            //    new { controller = "Admin", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            //).DataTokens.Add("Area", "Admin");
        }
        protected override void RegisterBundles(BundleCollection bundles)
        {
            base.RegisterBundles(bundles);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //BundleTable.EnableOptimizations = true;
        }

        public override void RegisterWebApi(HttpConfiguration config)
        {
            base.RegisterWebApi(config);
            WebApiConfig.Register(config);
        }
    }
}