﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sliverant.Web.Common;

namespace EPPW.Web.Areas.Documents.Controllers
{
    [AllowAnonymous]
    public class DevDocsController : BaseController
    {
        //
        // GET: /Documents/DevDosc/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DBCatchException()
        {
            return View();
        }

        public ActionResult BaseProjectConfig()
        {
            return View();
        }

    }
}
