﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sliverant.Web.Common;

namespace EPPW.Web.Areas.Documents.Controllers
{
    [AllowAnonymous]
    public class SampleController : BaseController
    {
        //
        // GET: /Documents/Sample/

        public ActionResult Index()
        {
            return View();
        }

    }
}
