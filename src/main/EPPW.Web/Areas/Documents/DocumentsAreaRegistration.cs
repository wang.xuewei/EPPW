﻿using System.Web.Mvc;

namespace EPPW.Web.Areas.Documents
{
    public class DocumentsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Documents";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Documents_default",
                "Documents/{controller}/{action}/{id}",
                new {controller="Sample", action = "Index", id = UrlParameter.Optional },
                new string[]{"EPPW.Web.Areas.Documents.Controllers"}
            );
        }
    }
}
