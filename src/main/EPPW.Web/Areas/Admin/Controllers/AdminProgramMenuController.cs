﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPPW.Membership.Service;
using EPPW.Membership.Dto;
using EPPW.Web.Common;
using Sliverant.Web.Common;

namespace EPPW.Web.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class AdminProgramMenuController : BaseController
    {
        // GET: /AdminMng/ProgramMenu/
        public IProgramMenuService ProgramMenuService { get; set; }
        public IProgramService ProgramService { get; set; }

        public ActionResult Index(string id)
        {
            List<ProgramMenuDto> result = ProgramMenuService.GetAllProgramMenuDto();

            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: /AdminMng/ProgramMenu/Create

        [HttpPost]
        public ActionResult Create(ProgramMenuDto programMenuDto)
        {
            try
            {
                // TODO: Add insert logic here
                ProgramMenuService.AddProgramMenuDto(programMenuDto);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /AdminMng/ProgramMenu/Edit/5

        public ActionResult Edit(int id)
        {
            ProgramMenuDto programMenuDto = ProgramMenuService.GetProgramMenuDto(id);
            return View(programMenuDto);
        }

        // POST: /AdminMng/ProgramMenu/Edit/5

        [HttpPost]
        public ActionResult Edit(ProgramMenuDto programMenuDto)
        {
            try
            {
                // TODO: Add update logic here

                ProgramMenuService.UpdateProgramMenuDto(programMenuDto);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /AdminMng/ProgramMenu/Delete/5

        public ActionResult Delete(int id)
        {
            ProgramMenuDto programMenuDto = ProgramMenuService.GetProgramMenuDto(id);
            return View(programMenuDto);
        }

        // POST: /AdminMng/ProgramMenu/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                ProgramMenuService.DeleteProgramMenuDtoById(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult UpdateProgramToMenu(int menuId,int programId)
        {
            int? programIdInt = programId;
            if (programId == 0)
                programIdInt = null;
            ProgramMenuService.UpdateProgramIdByMenuId(menuId, programIdInt);

            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        
        }

        public JsonResult GetAllProgramDtoList(int programMenuId,int programId)
        {
            List<ProgramDto> programDtoList = ProgramService.GetAllProgramDto();
            var result = from program in programDtoList
                         select new
                         {
                             check = String.Format("<input type=\"checkbox\" name=\"Programs\" value=\"{0}\" {1}>", program.ProgramId, program.ProgramId == programId ? "checked=\"checked\"" : "")
                             ,
                             Code = program.Code
                             ,
                             Name = program.Name
                         };

            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

     
    }
}
