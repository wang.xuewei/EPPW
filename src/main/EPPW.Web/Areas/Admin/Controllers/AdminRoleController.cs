﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using EPPW.Membership.Service;
using EPPW.Membership.Dto;
using Sliverant.Web.Common;

namespace EPPW.Web.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class AdminRoleController : BaseController
    {
        public IRoleService RoleService { private get; set; }

        // GET: /AdminMng/AdminRole/

        public ActionResult Index(string id)
        {
            List<RoleDto> result = RoleService.GetAllRoleDto();

            return View(result);
        }

    //    // GET: /AdminMng/AdminRole/Details/5

    //    public ActionResult Details(int id)
    //    {
    //        Role result = MembershipService.GetRole(id);
    //        return View(result);
    //    }

        public ActionResult RoleHasUsers(int id, string roleName)
        {
            List<UserDto> result = RoleService.GetUserDtoDtoByRoleId(id);
            ViewBag.RoleId = id;
            ViewBag.RoleName = roleName;
            return View(result);
        }
        public ActionResult RoleHasPrograms(int id,string roleName)
        {
            List<ProgramDto> result = RoleService.GetProgramDtoDtoByRoleId(id);
            ViewBag.RoleId = id;
            ViewBag.RoleName = roleName;
            return View(result);
        }

        // GET: /AdminMng/AdminRole/Create

        public ActionResult Create(string id)
        {

            return View();
        }

        // POST: /AdminMng/AdminRole/Create

        [HttpPost]
        public ActionResult Create(RoleDto role)
        {
            try
            {
                // TODO: Add insert logic here
                RoleService.AddRoleDto(role);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /AdminMng/AdminRole/Edit/5

        public ActionResult Edit(int id)
        {
            RoleDto result = RoleService.GetRoleDto(id);
            return View(result);
        }

        // POST: /AdminMng/AdminRole/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, RoleDto collection)
        {
            try
            {
                // TODO: Add update logic here
                RoleService.UpdateRoleDto(collection);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        // GET: /AdminMng/AdminRole/Delete/5

        public ActionResult Delete(int id)
        {
            RoleDto result = RoleService.GetRoleDto(id);
            return View(result);
        }

        // POST: /AdminMng/AdminRole/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                RoleService.DeleteRoleDtoById(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult Program(int id, string data)
        {
            List<ProgramDto> ProgramList = (new JavaScriptSerializer()).Deserialize<List<ProgramDto>>(data);

            foreach (ProgramDto ProgramPage in ProgramList)
            {
                if (ProgramPage.Status == DtoStatus.Delete)
                    RoleService.DeleteProgramDtoInRole(id, ProgramPage.ProgramId);

                if (ProgramPage.Status == DtoStatus.Add)
                    RoleService.AddProgramDtoInRole(id, ProgramPage.ProgramId);
            }

            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult User(int id, string data)
        {
            List<UserDto> roleHasUsers = RoleService.GetUserDtoDtoByRoleId(id);

            List<UserDto> UserList = (new JavaScriptSerializer()).Deserialize<List<UserDto>>(data);

            foreach (UserDto user in UserList)
            {
                UserDto Target = roleHasUsers.FirstOrDefault(c => c.UserId == user.UserId);
                if (user.Status == DtoStatus.Delete)
                    RoleService.DeleteUserDtoInRole(id, user.UserId);

                if (Target == null && user.Status == DtoStatus.Add)
                    RoleService.AddUserDtoInRole(id, user.UserId);
            }

            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

    }
}
