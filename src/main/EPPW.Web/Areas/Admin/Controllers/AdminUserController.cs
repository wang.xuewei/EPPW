﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections;
using EPPW.Membership.Service;
using EPPW.Membership.Dto;
using Sliverant.Web.Common;

namespace EPPW.Web.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class AdminUserController : BaseController
    {
        // GET: /AdminMng/AdminUser/
        public IUserService UserService { private get;set;}
        public IRoleService RoleService { private get; set; }

        public ActionResult Index(string id)
        {
            List<UserDto> result = UserService.GetAllUserDto();

            return View(result);
        }

        // GET: /AdminMng/AdminUser/Details/5

        public ActionResult Details(int id)
        {
            UserDto result = UserService.GetUserDto(id);

            return View(result);
        }

        public ActionResult UsersInRole(int id, string userName)
        {
            List<RoleDto> result = UserService.GetRoleDtoByUserId(id);
            ViewBag.UserName = userName;
            return View(result);
        }

        // GET: /AdminMng/AdminUser/Create

        public ActionResult Create(string id)
        {

            return View();
        }

        // POST: /AdminMng/AdminUser/Create

        [HttpPost]
        public ActionResult Create(UserDto user)
        {
            try
            {
                user.Password = user.ChkPassword;
                UserService.AddUserDto(user);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //// GET: /AdminMng/AdminUser/Edit/5

        public ActionResult Edit(int id)
        {
            UserDto result = UserService.GetUserDto(id);
            return View(result);
        }

        // POST: /AdminMng/AdminUser/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, UserDto user)
        {
            try
            {
                // TODO: Add update logic here
                UserService.UpdateUserDto(user);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /AdminMng/AdminUser/Delete/5

        public ActionResult Delete(int id)
        {
            UserDto result = UserService.GetUserDto(id);
            return View(result);
        }

        // POST: /AdminMng/AdminUser/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                UserService.DeleteUserDtoById(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult UserListByRole(int id)
        {
            List<UserDto> appusers = UserService.GetAllUserDto();
            var result = from user in appusers
                         select new
                         {
                             check = String.Format("<input type=\"checkbox\" name=\"Users\" value=\"{0}\" />", user.UserId)
                             ,
                             Name = user.UserName
                             ,
                             Desc = user.Email
                         };

            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

    }
}
