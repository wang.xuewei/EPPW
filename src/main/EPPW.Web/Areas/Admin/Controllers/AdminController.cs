﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using EPPW.Membership.Service;
using EPPW.Membership.Dto;
using Sliverant.Core.Utils;
using Sliverant.Web.Common;

namespace EPPW.Web.Areas.Admin.Controllers
{
    
    public class AdminController : BaseController
    {
        public ILoginService LoginService { private get; set; }
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Login(string inputEmail, string inputPassword, string lang)
        {
            if (String.IsNullOrEmpty(inputEmail) || String.IsNullOrEmpty(inputPassword))
                return View();

            //if (!MembershipService.HasUser())
            //    MembershipService.BaaSInit();

            try
            {
                FormsAuthentication.SignOut();
                SessionService.ClearAllSession();
                UserDto loginUser = LoginService.Login(inputEmail, inputPassword);
                SessionService.UserInfo = loginUser;
                CultureService.CurrentCultureName = lang;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorLoginMessage = ex.Message;
                return View();
            }

            if (SessionService.UserInfo != null)
            {
                FormsAuthentication.SetAuthCookie(inputEmail, false);
                return RedirectToAction("Index");
            }

            return View();
        }

        [AllowAnonymous]
        public void Logout()
        {
            FormsAuthentication.SignOut();
            SessionService.ClearAllSession();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
}
