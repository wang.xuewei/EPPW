﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPPW.Membership.Dto;
using EPPW.Membership.Service;
using Sliverant.Web.Common;

namespace EPPW.Web.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class AdminProgramController : BaseController
    {
        public IProgramService ProgramService { private get; set; }
        // GET: /AdminMng/AdminProgram/

        public ActionResult Index(string id)
        {
            List<ProgramDto> result = ProgramService.GetAllProgramDto();

            return View(result);
        }
 

        // GET: /AdminMng/AdminProgram/Details/5

        public ActionResult Details(int id)
        {
            ProgramDto programDto = ProgramService.GetProgramDtoById(id);

            return View(programDto);
        }

        public ActionResult ProgramsInRole(int id, string programName)
        {
            List<RoleDto> roleList = ProgramService.GetRoleDtoByProgramId(id);
            ViewBag.ProgramName = programName;
            return View(roleList);
        }

        // GET: /AdminMng/AdminProgram/Create

        public ActionResult Create(string id)
        {

            return View();
        }

        // POST: /AdminMng/AdminProgram/Create

        [HttpPost]
        public ActionResult Create(ProgramDto programDto)
        {
            try
            {
                // TODO: Add insert logic here
                //Modify By JiangYaFei 2014-8-12 14:29:55
                ProgramService.AddProgramDto(programDto, SessionService.UserInfo.UserId);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        // GET: /AdminMng/AdminProgram/Edit/5

        public ActionResult Edit(int id)
        {
            ProgramDto programDto = ProgramService.GetProgramDtoById(id);
            return View(programDto);
        }

        // POST: /AdminMng/AdminProgram/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, ProgramDto programDto)
        {
            try
            {
                // TODO: Add update logic here
                ProgramService.UpdateProgramDto(programDto, SessionService.UserInfo.UserId);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: /AdminMng/AdminProgram/Delete/5

        public ActionResult Delete(int id)
        {
            ProgramDto programDto = ProgramService.GetProgramDtoById(id);
            return View(programDto);
        }

        // POST: /AdminMng/AdminProgram/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                ProgramService.DeleteProgramDtoById(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult ProgramListByRole()
        {
            List<ProgramDto> appPrograms = ProgramService.GetAllProgramDto();

            var result = from program in appPrograms
                         select new
                         {
                             check = String.Format("<input type=\"checkbox\" name=\"Programs\" value=\"{0}\">", program.ProgramId)
                             ,
                             Code = program.Code
                             ,
                             Name = program.Name
                             ,
                             Desc = program.Description
                         };

            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
    }
}
