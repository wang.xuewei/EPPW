﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sliverant.Web.Common;

namespace EPPW.Web.Controllers
{
    [AllowAnonymous]
    public class QuickStartController : BaseController
    {
        /// <summary>
        /// Action for View to introduce JS-Plugin "bootstrap-datetimepicker"
        /// </summary>
        /// <returns></returns>
        public ActionResult DatetimePicker()
        {
            return View();
        }
        /// <summary>
        /// Action for View to introduce JS-Plugin "DataTables"
        /// </summary>
        /// <returns></returns>
        public ActionResult DataTables()
        {
            return View();
        }

        //Ajax
        public JsonResult GetProductList()
        {
            //List<object> result = new List<object>() { 
            //    new { ProductName="AA羽绒服",Price=599.00,Sales=258 },
            //    new { ProductName="AC运动鞋",Price=369.00,Sales=662 },
            //    new { ProductName="TT围巾",Price=128.90,Sales=773 },
            //};
            List<object> result = new List<object>();
            for (int i = 0; i <= 93; i++)
            {
                var tempObj = new { 
                    ProductName=i.ToString()+"运动鞋",
                    Price=1377-i*10,
                    Sales=i*10+9
                };
                result.Add(tempObj);
            }

            return Json(new { aaData=result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Hogan()
        {
            return View();
        }

        public ActionResult Highcharts()
        {
            return View();
        }

        public ActionResult MessageBox()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PopView()
        {
            return View();
        }
        public ActionResult ContentOfPopView(string name, string param1)
        {
            ViewBag.dd = param1;
            ViewBag.name = name;
            return View();
        }

    }
}
