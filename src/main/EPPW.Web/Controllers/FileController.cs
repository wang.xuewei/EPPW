﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPPW.Web.Common;
using EPPW.Collections;
using System.IO;
using Sliverant.Web.Common;
using EPPW.FileMng.Dto;
using EPPW.FileMng.Service;

namespace EPPW.Web.Controllers
{
    [AllowAnonymous]
    public class FileController : EPPWBaseController
    {
        public IFileService FileService;

        public string fileServerDirectory {
            get {
                return Server.MapPath(@"~");
            }
        }
        public FileStreamResult GetFile(int id)
        {
            //IDictionary<int, FileDto> filelist = new Dictionary<int, FileDto>();

            //filelist.Add(1, new FileDto() { FileName = "衣服.bmp", FileExtName = "bmp" });
            //filelist.Add(2, new FileDto() { FileName = "衣服.gif", FileExtName = "gif" });
            //filelist.Add(3, new FileDto() { FileName = "衣服.jpg", FileExtName = "jpg" });
            //filelist.Add(4, new FileDto() { FileName = "衣服.png", FileExtName = "png" });
            //filelist.Add(5, new FileDto() { FileName = "衣服.tif", FileExtName = "tif" });
            //filelist.Add(6, new FileDto() { FileName = "server.ico", FileExtName = "ico" });


            //return File(new FileStream(Server.MapPath(@"~/upload/" + filelist[id].FileName), FileMode.Open), "application/octet-stream", filelist[id].FileName + "." + filelist[id].FileExtName);


            NameValueList args = new NameValueList();
            args.Add("FileID", id);
            FileDto fileDto = FileService.GetFileByFileID(args);
            if (fileDto != null)
            {

                return File(new FileStream(fileServerDirectory+fileDto.FilePath, FileMode.Open), "application/octet-stream", fileDto.FileName);
            }
            else {
                Response.Redirect("~/Error/Http500");
                return null;
            }
        
        }

        public JsonResult UploadFile()
        {
            
                List<FileDto> fileList = new List<FileDto>();

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    FileDto fileDto = new FileDto();
                    HttpPostedFileBase file = Request.Files[i];

                    string fileUploadDir = @"upload/";
                    if (!Directory.Exists(Path.GetDirectoryName(fileUploadDir)))
                    {
                        Directory.CreateDirectory(fileServerDirectory+fileUploadDir);
                    }
                    file.SaveAs(fileServerDirectory + fileUploadDir + file.FileName);
                    fileDto.FileExtName = file.FileName.Substring(file.FileName.LastIndexOf(".")+1, file.FileName.Length - (file.FileName.LastIndexOf(".")+1));
                    fileDto.FileName = file.FileName;
                    fileDto.FilePath = fileUploadDir + file.FileName;
                    fileDto.InUserID = 0;
                    NameValueList args = new NameValueList();
                    args.Add("FileDto", fileDto);

                    fileDto = FileService.SaveFile(args);
                    fileList.Add(fileDto);
                }

                return Json(JsonResultType.Success, "", new { fileList = fileList }, JsonRequestBehavior.AllowGet);
           
        }

        public ActionResult WebUpload(string extensions, string numLimit)
        {
            ViewBag.extensions = extensions;
            if (string.IsNullOrEmpty(numLimit))
            {
                ViewBag.numLimit = int.MaxValue;
            }
            else
            {
                ViewBag.numLimit = numLimit;
            }
            return View();
        }
    }
}
