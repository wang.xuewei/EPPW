﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPPW.SampleMng.Service;
using EPPW.Web.Common;
using System.IO;
using System.Threading;
using EPPW.Web.App_Code;
using Sliverant.Web.Common;
using EPPW.Collections;

namespace EPPW.Web.Controllers
{
    [AllowAnonymous]
    public class SampleController : EPPWBaseController
    {
        public ISampleService SampleService { private get; set; }
        //
        // GET: /Sample/
        [AllowAnonymous]
        public ActionResult Index()
        {
            EPPW.Web.Common.Helper.CultureHelper.InitCulture(CultureService);
            ViewBag.Date = SampleService.GetDate();
            //CultureProvider.CurrentCultureName = "ko-KR";
            return View();
        }
        [AllowAnonymous]
        public JsonResult SearchTest(string param1)
        {

            List<string> testList = new List<string> { "aa", "bb", "cc", "dd" };
            return Json(JsonResultType.Success, "", new { items = testList }, JsonRequestBehavior.AllowGet);



        }

        [AllowAnonymous]
        public ActionResult ExtendMethod()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult File()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult GuideTest()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult UEditor()
        {
            return View();
        }
        [AllowAnonymous]
        public JsonResult UEditorSubmit(string html)
        {


            return Json(JsonResultType.Success, "", new { HTML = html }, JsonRequestBehavior.AllowGet);



        }
        [AllowAnonymous]
        public ActionResult Uploadify()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult WebUpload()
        {
            return View();
        }
        [AllowAnonymous]
        public JsonResult UploadFile()
        {


            for (int i = 0; i < Request.Files.Count; i++)
            {

                HttpPostedFileBase file = Request.Files[i];

                string fileUploadDir = Server.MapPath(@"~/upload/");
                if (!Directory.Exists(Path.GetDirectoryName(fileUploadDir)))
                {
                    Directory.CreateDirectory(fileUploadDir);
                }
                file.SaveAs(fileUploadDir + file.FileName);

            }

            return Json(JsonResultType.Success, "", new { HTML = "" }, JsonRequestBehavior.AllowGet);



        }
        public ActionResult ValidateTest2()
        {
            return View();
        }

        public ActionResult Select()
        {
            return View();
        }
        public ActionResult Brand()
        {
            return View();
        }
        public ActionResult Item()
        {
            return View();
        }
        public ActionResult Style()
        {
            return View();
        }

        public ActionResult Training()
        {
            return View();
        }
        public ActionResult Boot3Sample()
        {
            return View();
        }

        public ActionResult jqgrid()
        {
            return View();
        }

        public ActionResult hogantemp()
        {
            return View();
        }
        public ActionResult Chart_Line()
        {
            return View();
        }
        public ActionResult jqPlot_Bar()
        {
            return View();
        }
        public ActionResult Echart_Bar()
        {
            return View();
        }
        public ActionResult IconTemp()
        {
            return View();
        }
        public ActionResult AjaxSample()
        {

            return View();
        }
        public JsonResult AjaxData()
        {

            Thread.Sleep(5000);
            return Json(JsonResultType.Success, "", new { Data = "1234" }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ExceptionSample()
        {
            return View();
        }
        public JsonResult ExceptionAccess()
        {

            SampleService.GetSqlException();

            return null;
        }
        public ActionResult DataUploadTest()
        {
            return View();
        }
        public ActionResult HomePage()
        {
            return View();
        }
    }
}
