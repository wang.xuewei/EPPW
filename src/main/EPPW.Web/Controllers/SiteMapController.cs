﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPPW.Web.Common.Models;
using Sliverant.Web.Common;
using EPPW.Membership.Service;
using EPPW.Membership.Dto;
using Sliverant.Web.Common.Utils;
using EPPW.Web.Controllers;


namespace EPPW.Web.Controllers
{


    [AllowAnonymous]
    public class SiteMapController : EPPWBaseController
    {
        public IProgramMenuService ProgramMenuService { get; set; }
        //
        // GET: /SiteMap/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult TopMenu()
        {
            string menuName = "";
            IEnumerable<SiteMapNodeModel> result = GetTopMenu(menuName);

            SiteMapNodeModel siteMapNodeModel = result.FirstOrDefault();
            ViewBag.userName = SessionService.UserInfo == null ? null : SessionService.UserInfo.Name;//Add By JiangYafei 2015-2-2 14:44:42
            return View(result.First());
        }

        public ActionResult SideMenu(string action, string controller, string parentDepth)
        {
            IEnumerable<SiteMapNodeModel> result = GetSideMenu(action, controller, parentDepth);

            SiteMapNodeModel siteMapNodeModel = result.FirstOrDefault();
            return View(result.First());
        }

        /// <summary>
        /// DESC: GET All Menu 
        /// CREATOR:jin.chunhe
        /// CREATE-DATE:2014-09-16
        /// </summary>
        /// <returns></returns>
        public List<ProgramMenuDto> GetAllMenu()
        {
            List<ProgramMenuDto> menulist = SessionService.GetValue("SESSION_SITEMENU_ALLMENU") as List<ProgramMenuDto>;
            if (menulist == null)
            {
                menulist = ProgramMenuService.GetAllProgramMenuDto();
                SessionService.SetValue("SESSION_SITEMENU_ALLMENU", menulist);
            }
            return menulist;
        }
        /// <summary>
        /// DESC: GET Authority Menu 
        /// CREATOR:kuang.yongsheng
        /// CREATE-DATE:2015-01-07
        /// </summary>
        /// <returns></returns>
        public List<ProgramMenuDto> GetAuthorityMenu()
        {
            List<ProgramMenuDto> menulist = SessionService.GetValue("SESSION_SITEMENU_AuthorityMENU") as List<ProgramMenuDto>;
            if (menulist == null)
            {
                if (SessionService.UserInfo != null)
                {
                    menulist = ProgramMenuService.GetProgramMenuByUserId(SessionService.UserInfo.UserId);
                    SessionService.SetValue("SESSION_SITEMENU_AuthorityMENU", menulist);
                }
            }
            return menulist;
        }


        /// <summary>
        /// DESC: GetTopMenu
        /// CREATOR:jin.chunhe
        /// CREATE-DATE:2014-09-16
        /// </summary>
        /// <param name="menuName"></param>
        /// <returns></returns>
        public IEnumerable<SiteMapNodeModel> GetTopMenu(string menuName)
        {
            //Guid appid = new Guid("E23756DA-E5DB-4AF0-9167-A1D401746F31");
            //int parentId = 0;
            // List<ProgramMenu> menulist = this.GetAllMenu();
            //修改为只能看到有权限的页面
            List<ProgramMenuDto> menulist = this.GetAuthorityMenu();

            List<ProgramMenuDto> topmenulist = new List<ProgramMenuDto>();
            SiteMapNodeModelList topMenuModelList = new SiteMapNodeModelList();
            if (menulist != null)
            {
                topmenulist = menulist.FindAll(p => p.ParentId == 0);
            }
            if (topmenulist != null)
            {
                foreach (ProgramMenuDto pmenu in topmenulist)
                {
                    topMenuModelList.Add(new SiteMapNodeModel
                    {
                        Action = pmenu.Action,
                        Controller = pmenu.Controller,
                        Title = pmenu.Name,
                        Description = pmenu.Description
                    });
                }
            }
            SiteMapNodeModelList result = new SiteMapNodeModelList();
            result.Add(new SiteMapNodeModel { Children = topMenuModelList });
            return result;
        }
        /// <summary>
        /// DESC: GetSideMenu
        /// CREATOR:jin.chunhe
        /// CREATE-DATE:2014-09-16
        /// </summary>
        /// <param name="action"></param>
        /// <param name="controller"></param>
        /// <param name="parentDept"></param>
        /// <returns></returns>
        public IEnumerable<SiteMapNodeModel> GetSideMenu(string action, string controller, string parentDept)
        {
            //List<ProgramMenu> menulist = this.GetAllMenu();
            //修改为只能看到有权限的页面
            List<ProgramMenuDto> menulist = this.GetAuthorityMenu();

            SiteMapNodeModelList folderProgModelList = new SiteMapNodeModelList();

            //ProgramMenu thisMenu = menulist.Find(p => p.ProgramPage != null
            //    && p.ProgramPage.WebProgram.Action == action
            //    && p.ProgramPage.WebProgram.Controller == controller
            //    && p.ParentId != 0);
            //if (thisMenu != null)
            //{
            //   ProgramMenu folderMenu = menulist.Find(p => p.Id == thisMenu.ParentId);//查询当前页面菜单所属文件夹

            //ProgramMenu topMenu = menulist.Find(p => p.ProgramPage.WebProgram.Controller == controller);//查询当前文件夹所属TopMenu
            //ProgramMenuDto topMenu = menulist.Find(p => p.Controller == controller && p.Action == action);
            ProgramMenuDto topMenu = null;
            foreach (ProgramMenuDto topmenu1 in menulist)
            {
                foreach (ProgramMenuDto menufolder in menulist.FindAll(p => p.ParentId == topmenu1.Id))
                {
                    ProgramMenuDto currentmenu = menulist.FindAll(p => p.ParentId == menufolder.Id).Find(p => p.Controller == controller && p.Action == action);
                    if (currentmenu != null)
                    {
                        topMenu = topmenu1;
                        break;
                    }
                }
                if (topMenu != null)
                {
                    break;
                }
            }

            if (topMenu != null)
            {
                List<ProgramMenuDto> oFolderList = menulist.FindAll(p => p.ParentId == topMenu.Id);//oFolderList：所有2级菜单
                oFolderList.Sort((left, right) =>
                {
                    if (left.DisplaySeq > right.DisplaySeq)
                        return 1;
                    else if (left.DisplaySeq == right.DisplaySeq)
                        return 0;
                    else
                        return -1;
                });
                foreach (ProgramMenuDto folderprogMenu in oFolderList)
                {
                    List<ProgramMenuDto> progList = menulist.FindAll(p => p.ParentId == folderprogMenu.Id); //progList：所有3级菜单
                    if (progList != null)
                    {
                        progList.Sort((left, right) =>
                        {
                            if (left.DisplaySeq > right.DisplaySeq)
                                return 1;
                            else if (left.DisplaySeq == right.DisplaySeq)
                                return 0;
                            else
                                return -1;
                        });
                        SiteMapNodeModel folderModel = new SiteMapNodeModel();
                        SiteMapNodeModelList programModelList = new SiteMapNodeModelList();
                        foreach (ProgramMenuDto progMenu in progList)
                        {
                            if (progMenu != null)
                            {
                                programModelList.Add(new SiteMapNodeModel
                                {
                                    Action = progMenu.Action,
                                    Controller = progMenu.Controller,
                                    Title = progMenu.Name,
                                    Description = progMenu.Description,
                                    IsInCurrentPath = (progMenu.Action == action
                                    && progMenu.Controller == controller)
                                });
                            }
                        }

                        folderModel.Title = folderprogMenu.Name;
                        folderModel.Description = folderprogMenu.Description;
                        folderModel.Children = programModelList;
                        if (programModelList != null && programModelList.Count != 0)//如果没有三级菜单，也不要显示二级菜单
                        {
                            folderProgModelList.Add(folderModel);
                        }
                    }
                }
            }
            //}
            SiteMapNodeModelList result = new SiteMapNodeModelList();
            result.Add(new SiteMapNodeModel { Children = folderProgModelList });
            return result;
        }
        /// <summary>
        /// DESC:SetCulture
        /// CREATOR:jin.chunhe
        /// CREATE-DATE:2014-09-05
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult SetCulture(string culture, string returnUrl)
        {
            if (!string.IsNullOrEmpty(culture))
            {
                //System.Globalization.CultureInfo cult = new System.Globalization.CultureInfo(culture);
                ////ResourceProvider.
                //StorageProvider.SetData(Values.NAME_SESSION_CULTURE, cult.Name, LifeCycle.Session);
                //CultureProvider.SetCulture(cult.Name);
                CultureService.CurrentCultureName = culture;
            }
            return Json(JsonResultType.Success, "", new { resReturnUrl = returnUrl }, JsonRequestBehavior.AllowGet);
        }
    }
}
