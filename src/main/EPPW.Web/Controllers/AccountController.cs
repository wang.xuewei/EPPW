﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sliverant.Web.Common;
using System.Web.Security;
using EPPW.Membership.Service;
using EPPW.Membership.Dto;
using Sliverant.Web.Common.Utils;

namespace EPPW.Web.Controllers
{
    public class AccountController : BaseController
    {

        #region member(s)

        //public IMembershipService MembershipService { get; set; }
        public ILoginService LoginService { private get; set; }

        #endregion

        #region login

        [AllowAnonymous]
        public ActionResult Login()
        {
            InitCulture();
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string inputUserID, string inputPassword, string language)
        {
            try
            {
                InitViewState();
                UserDto loginUser = LoginService.Login(inputUserID, inputPassword);
                SessionService.UserInfo = loginUser;
            }
            catch (Exception ex)
            {
                ViewBag.ErrorLoginMessage = ex.Message;
                return View();
            }
            finally
            {
                InitCulture();
            }

            if (SessionService.UserInfo.UserId != 0)
            {
                FormsAuthentication.SetAuthCookie(inputUserID, false);
                return Redirect("/Home/HomePage");
            }

            return View();
        }
        [AllowAnonymous]
        public ActionResult Logout()
        {
            InitViewState();
            return Redirect("/Account/Login");
        }

        #endregion

        #region private method

        private void ClearAllCookie()
        {
            HttpCookie aCookie;
            string cookieName;
            int limit = Request.Cookies.Count;
            for (int i = 0; i < limit; i++)
            {
                cookieName = Request.Cookies[i].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);
            }
        }
        private string GetBrowseLang()
        {
            string[] langs = Request.UserLanguages;
            string firstLang = string.Empty;
            if (langs.Length > 0)
            {
                firstLang = langs[0];
                switch (langs[0])
                {
                    case "zh":
                        firstLang = "zh-CN";
                        break;
                    case "en":
                        firstLang = "en-US";
                        break;
                    case "ko":
                        firstLang = "ko-KR";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                firstLang = "en-US";
            }

            return firstLang;
        }
        private void InitCulture()
        {
            string cultureName = GetBrowseLang();
            CultureService.CurrentCultureName = cultureName;
        }
        private void InitViewState()
        {
            FormsAuthentication.SignOut();
            SessionService.ClearAllSession();
        }

        #endregion

    }
}
