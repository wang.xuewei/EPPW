﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPPW.Collections;
using Sliverant.Web.Common;
using EPPW.HomeMng.Service;


namespace EPPW.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : EPPWBaseController
    {
        //
        // GET: /Home/
        public IHomeService HomeService { get; set; }
        public ActionResult Index()
        {
            return View();
        }
       
        public ActionResult HomePage()
        {
            return View();
        }

        public JsonResult SaveInvest(string chk1, string chk2, string chk3, string chk4, string chk5, string chk6, string chk7, string email, string tel, string remark)
        {
            NameValueList param = new NameValueList();
            param.Add("Chk1", chk1);
            param.Add("Chk2", chk2);
            param.Add("Chk3", chk3);
            param.Add("Chk4", chk4);
            param.Add("Chk5", chk5);
            param.Add("Chk6", chk6);
            param.Add("Chk7", chk7);
            param.Add("Email", email);
            param.Add("Tel", tel);
            param.Add("Remark", remark);
            HomeService.SaveInvest(param);
            return Json(JsonResultType.Success, "", new { }, JsonRequestBehavior.AllowGet);
        }
    }
}
