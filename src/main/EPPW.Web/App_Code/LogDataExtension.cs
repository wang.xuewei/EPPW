﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sliverant.Support.Log;


/// <summary>
/// Summary description for LogDataExtension
/// </summary>
public class LogDataExtension : ILogDataExtension
{
    public LogDataExtension()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region ILogDataExtension Members

    public LogEventData GetExtraData()
    {
        LogEventData logEvnetData = new LogEventData();
        //Sliverant.Agent.Logging.Transfer.IHeaderContext data = (Sliverant.Agent.Logging.Transfer.IHeaderContext)System.Runtime.Remoting.Messaging.CallContext.GetData("ClientHeaderKey");
        //if (data == null)
        //{
        //    return logEvnetData;
        //}
        //logEvnetData.UserId = data.ClientInfo.UserId;
        //logEvnetData.ProgramId = data.ClientInfo.ProgramId;
        //logEvnetData.ClientIP = data.ClientInfo.IPAddress;
        logEvnetData.ClientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        HttpBrowserCapabilities bc = HttpContext.Current.Request.Browser;
        logEvnetData.ClientAppType = bc.Type;
        return logEvnetData;
        return logEvnetData;
    }


    #endregion
}
