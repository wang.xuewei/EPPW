﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Common.Logging;
using System.Collections.Generic;
using Newtonsoft.Json;

/// <summary>
/// Summary description for MsmqGateway
/// </summary>
public class MsmqGateway : Spring.Messaging.Core.MessageQueueGatewaySupport
{
    ILog log = LogManager.GetLogger(typeof(MsmqGateway));

    public MsmqGateway()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void Send(IDictionary<string, object> logmessage)
    {
        MessageQueueTemplate.ConvertAndSend(JsonConvert.SerializeObject(logmessage));
    }
}

