﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Sliverant.Core.Utils;
using Sliverant.Core.Exceptions;

namespace EPPW.Web.App_Code
{
    public class ServerExceptionAdvice : Spring.Aop.IThrowsAdvice
    {
        private const int INDEX_ERROR_CODE = 0;
        private const int INDEX_ERROR_SPNAME = 1;
        private const int INDEX_ERROR_PARAM_START = 2;
        public IResourceHelper ResourceService { get; set; }

        public void AfterThrowing(Exception ex)
        {
            BaseException sliverantException = null;
            try
            {
                if (ex is NHibernate.ADOException && ex.InnerException != null &&
                    ex.InnerException is SqlException && String.IsNullOrEmpty(ex.InnerException.Message) == false)
                {
                    string[] errMsgs = ex.InnerException.Message.Split('|');
                    if (errMsgs.Length < 3)
                    {
                        sliverantException = new Sliverant.Core.Exceptions.SystemException(ex.Message, ex.InnerException);
                    }
                    else
                    {
                        if (errMsgs.Length == 3 && String.IsNullOrEmpty(errMsgs[INDEX_ERROR_PARAM_START]))
                        {
                            if (errMsgs[INDEX_ERROR_CODE] == "SystemError")
                            {
                                sliverantException = new Sliverant.Core.Exceptions.SystemException(ex.InnerException.Message, ex.InnerException);
                            }
                            else
                            {
                                sliverantException = new WarnningException(ResourceService.GetMessage(errMsgs[INDEX_ERROR_CODE]), ex.InnerException, errMsgs[INDEX_ERROR_CODE]);
                            }
                        }
                        else
                        {
                            List<string> paramList = new List<string>(errMsgs);
                            paramList.RemoveRange(0, 2);
                            for (int i = 0; i < paramList.Count; i++)
                            {
                                paramList[i] = ResourceService.GetMessage(paramList[i]) ?? string.Empty;
                            }
                            sliverantException = new WarnningException(ResourceService.GetMessage(errMsgs[INDEX_ERROR_CODE], paramList.ToArray())
                                , ex.InnerException, errMsgs[INDEX_ERROR_CODE], paramList.ToArray());
                        }
                    }

                }
            }
            catch
            {
                throw ex;
            }
            if (sliverantException == null) throw ex;
            throw sliverantException;
        }
    }
}
