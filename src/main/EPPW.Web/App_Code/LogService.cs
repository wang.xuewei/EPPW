﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common.Logging;
using System.Collections.Generic;
using Sliverant.Support.Log;

/// <summary>
/// Summary description for LogService
/// </summary>
public class LogService : ILogService
{
    ILog log = LogManager.GetLogger(typeof(LogService));
    private MsmqGateway _msmqGateway;

    public MsmqGateway MsmqGateway
    {
        set
        {
            _msmqGateway = value;
        }
        private get
        {
            return _msmqGateway;
        }
    }

    public void Send(IDictionary<string, object> logMessage)
    {
        try
        {
            MsmqGateway.Send(logMessage);
        }
        catch (Exception e)
        {
            log.Error(e);
        }
    }
}
