﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.IO;
using System.Collections.Specialized;
using System.Configuration;
using System.Web.Configuration;
using System.Xml;
using EPPW.Collections;


namespace EPPW.Web
{
    public class BundleConfig : IConfigurationSectionHandler
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
            //1.common bundle
            StyleBundle commonStyleBundle = new StyleBundle("~/bundles/common/style/RoleModule");
            ScriptBundle commonScriptBundle = new ScriptBundle("~/bundles/common/script/RoleModule");
            commonStyleBundle.IncludeDirectory("~/Content/public/bootstrap", "*.css", true);
            commonStyleBundle.IncludeDirectory("~/Content/public/bootstrap/images", "*.png", true);
            commonStyleBundle.IncludeDirectory("~/Content/StyleSheets", "*.css", true);
            commonStyleBundle.Include("~/Content/images/public/ajax-loader.gif");

            commonScriptBundle.Include("~/Scripts/Public/jquery-1.9.1.js");
            //commonScriptBundle.IncludeDirectory("~/Scripts/Public/bootstrap", "*.js", true);


            //add bootstrap3 add by xiao.xinmiao
            StyleBundle commonStyleBundle3 = new StyleBundle("~/bundles/common/style");
            ScriptBundle commonScriptBundle3 = new ScriptBundle("~/bundles/common/script");
            //commonStyleBundle3.IncludeDirectory("~/Content/public/bootstrap3", "*.css", true);

            commonStyleBundle3.Include("~/Content/public/bootstrap3/bootstrap.css");
            //commonStyleBundle3.Include("~/Content/public/bootstrap3/bootstrap-theme.css");
            commonStyleBundle3.Include("~/Content/public/bootstrap3/bootstrap-custom.css");

            commonStyleBundle3.IncludeDirectory("~/Content/StyleSheets", "*.css", true);
            commonStyleBundle3.IncludeDirectory("~/Content/StyleSheets", "*.css", true);


            commonStyleBundle3.Include("~/Content/plugins/dialog/jquery.modaldialog.css");//Add MessageBox(css)

            commonScriptBundle3.Include("~/Scripts/Public/jquery-1.11.1.js");
            commonScriptBundle3.IncludeDirectory("~/Scripts/Public/bootstrap3", "*.js", true);

            commonScriptBundle3.Include("~/Scripts/Public/common/common.js");
            commonScriptBundle3.Include("~/Scripts/Public/resource.js");
            commonScriptBundle3.Include("~/Scripts/Public/i18next-1.6.0.js");
            commonScriptBundle3.Include("~/Scripts/Public/spin.js");
            commonScriptBundle3.Include("~/Scripts/Public/sa/jquery.sa.ajax.js");
            commonScriptBundle3.Include("~/Scripts/Public/sa/jquery.sa.dialog.js");//Add MessageBox(js)

            BundleTable.Bundles.Add(commonStyleBundle);
            BundleTable.Bundles.Add(commonScriptBundle);

            BundleTable.Bundles.Add(commonStyleBundle3);
            BundleTable.Bundles.Add(commonScriptBundle3);

            //2.get custom bundle from web.config
            List<SimpleDto> nvList = (List<SimpleDto>)ConfigurationManager.GetSection("bundleGroup/bundle");
            if (nvList == null) return;
            string bundleKey;
            foreach (SimpleDto simpleDto in nvList)
            {
                bundleKey = simpleDto.Desc;
                Bundle bundle;
                if (!string.IsNullOrEmpty(simpleDto.Param))
                {
                    bundle = CreateOrDefaultBundle(bundleKey, simpleDto.Code);
                    bundle.Include(bundleKey + "/" + simpleDto.Param);
                    BundleTable.Bundles.Add(bundle);
                    continue;
                }
                else
                {
                    switch (bundleKey.ToLower())
                    {
                        case "js":
                            bundle = new ScriptBundle(bundleKey);
                            bundle.IncludeDirectory(bundleKey, "*." + simpleDto.Code, true);
                            break;

                        default:
                            bundle = CreateOrDefaultBundle(bundleKey, simpleDto.Code);
                            bundle.IncludeDirectory(bundleKey, "*." + simpleDto.Code, true);
                            break;
                    }
                    BundleTable.Bundles.Add(bundle);
                }
            }
        }


        private static Bundle CreateOrDefaultBundle(string bundlePath, string bundleType)
        {
            Bundle bundle;
            bundle = BundleTable.Bundles
            .FirstOrDefault<Bundle>(p => p.Path == bundlePath);
            if (bundle == null)
            {
                if (bundleType == "js")
                {
                    bundle = new ScriptBundle(bundlePath);
                }
                else
                {
                    bundle = new StyleBundle(bundlePath);
                }
            }
            return bundle;
        }

        #region IConfigurationSectionHandler Members

        /// <summary>
        /// 获取sectionGroup的信息 add by xiao.xinmiao 2014.08.15
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            List<SimpleDto> codeNameList = new List<SimpleDto>();
            foreach (XmlNode childNode in section.ChildNodes)
            {
                if (childNode.NodeType != XmlNodeType.Comment && childNode.Attributes["suffixName"] != null)
                {
                    SimpleDto SimpleDto = new SimpleDto();
                    SimpleDto.Code = childNode.Attributes["suffixName"].Value;

                    if (childNode.Attributes["plugPath"] != null)
                    {
                        SimpleDto.Desc = childNode.Attributes["plugPath"].Value;
                    }
                    else
                    {
                        SimpleDto.Desc = string.Empty;
                    }
                    if (childNode.Attributes["plugFileName"] != null)
                    {
                        SimpleDto.Param = childNode.Attributes["plugFileName"].Value;
                    }
                    codeNameList.Add(SimpleDto);
                }
            }
            return codeNameList;
        }

        #endregion

    }
}