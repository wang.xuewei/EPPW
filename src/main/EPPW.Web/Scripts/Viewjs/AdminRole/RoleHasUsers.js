﻿ var modifyUserList = [];
 $(function () {

     var roleId = $("#divRoleId > #roleId").val();

     $('#AddUserModal').on('show', function () {

         $('#AddUserTable').dataTable(
                {
                    "bRetrieve": true,
                    "bProcessing": true,
                    "sAjaxSource": '/Admin/AdminUser/UserListByRole/' + roleId,
                    "fnDrawCallback": function () {
                        setChecked();
                        $("#AddUserTable>tbody").find(":checkbox").on("change", function () {
                            var $this = $(this);
                            var isFind = false;
                            var FindItem;
                            for (var i = 0; i < modifyUserList.length; i++) {
                                if (modifyUserList[i].id == $this.val()) {
                                    isFind = true;
                                    FindItem = modifyUserList[i];
                                    break;
                                }
                            }
                            if (isFind == false) modifyUserList.push({ UserId: $this.val(), Status: $this.is(":checked") ? 1 : 2 })
                            else {
                                modifyUserList.pop(FindItem);
                            }

                        });
                    },
                    "aoColumns": [
                        { "mData": "check" },
                        { "mData": "Name" },
                        { "mData": "Desc" }
                    ]
                });

     })

     $("#btnAddUsers").on("click", function () {
         var url = '/Admin/AdminRole/User/' + roleId;

         $.post(url, { data: JSON.stringify(modifyUserList) }, function () {
             $('#AddUserModal').modal('hide');
             window.location.reload(true);
         }, "json");
     });

     $('#AddUserModal').on("hide", function () {
         setChecked();
     })
     function setChecked() {
         var checkboxList = $("#AddUserTable>tbody").find(":checkbox");
         if (checkboxList.length == 0) return;
         checkboxList.each(function () {
             var roleUserIdList = $("#divRoleUserId").find("input[name='roleUserId']");
             for (var i = 0; i < roleUserIdList.length; i++) {
                 $(this)[0].checked = false;
                 if ($(this).val() == $(roleUserIdList[i]).val()) {
                     $(this)[0].checked = true;
                     break;
                 }
             }
         });
     }
 });