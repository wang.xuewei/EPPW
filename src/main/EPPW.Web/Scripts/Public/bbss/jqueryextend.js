/// <reference path="../jquery-1.11.1.js" />

//;

(function ($) {
    $.sa = $.sa || {};
    $.sa.rightString = function (str, length) {
        var startIndex = (str.length - length) > 0 ? (str.length - length) : 0;
        return str.substr(startIndex, length);
    };
    $.sa.leftString = function (str, length) {
        return str.substr(0, length);
    };
    $.sa.randomNumber = function (min, max) {
        //包含 min，但是不包含max
        return Math.random() * (max - min) + min;
    };
    $.sa.randomInt = function (min, max) {
        //包含 min，但是不包含max
        return Math.floor(Math.random() * (max - min) + min);
    };
    $.sa.commaToNum = function (comma) {
        return $.trim(comma).replace(/\,/g, "");
    };

    $.sa.numToComma = function (num, decimalLength) {
        num = parseFloat(num);
        if (isNaN(num))
            return  null;
        var outputInt = function (number) {
            if (number.length <= 3)
                return (number == '' ? '0' : number);
            else {
                var mod = number.length % 3;
                var output = (mod == 0 ? '' : (number.substring(0, mod)));
                for (i = 0; i < Math.floor(number.length / 3); i++) {
                    if ((mod == 0) && (i == 0))
                        output += number.substring(mod + 3 * i, mod + 3 * i + 3);
                    else output += ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);
                }
                return (output);
            }
        }
        var outputDecimal = function (amount) {
            var decimaldot = "."
            if (decimalLength === 0) {
                decimaldot = "";
            }
            else if (!decimalLength)
                decimalLength = 2;

            amount = Math.round(((amount) - Math.floor(amount)) * Math.pow(10, decimalLength));
            amount = "000000000000000000000" + amount;

            return decimaldot + $.sa.rightString(amount, decimalLength);
        }

        if (num < 0) {
            return '-' + outputInt(Math.floor(Math.abs(num) - 0) + '') + outputDecimal(Math.abs(num) - 0, decimalLength);
        } else {
            return outputInt(Math.floor(num - 0) + '') + outputDecimal(num - 0, decimalLength);
        }

    };
    $.sa.parseDate = function (format, date, newformat, opts) {
        var token = /\\.|[dDjlNSwzWFmMntLoYyaABgGhHisueIOPTZcrU]/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		msDateRegExp = new RegExp("^\/Date\\((([-+])?[0-9]+)(([-+])([0-9]{2})([0-9]{2}))?\\)\/$"),
		msMatch = ((typeof date === 'string') ? date.match(msDateRegExp) : null),
		pad = function (value, length) {
		    value = String(value);
		    length = parseInt(length, 10) || 2;
		    while (value.length < length) { value = '0' + value; }
		    return value;
		},
		ts = { m: 1, d: 1, y: 1970, h: 0, i: 0, s: 0, u: 0 },
		timestamp = 0, dM, k, hl,
		h12to24 = function (ampm, h) {
		    if (ampm === 0) { if (h === 12) { h = 0; } }
		    else { if (h !== 12) { h += 12; } }
		    return h;
		};
        if (opts === undefined) {
            opts = $.sa.formatter.date;
        }
        // old lang files
        if (opts.parseRe === undefined) {
            opts.parseRe = /[#%\\\/:_;.,\t\s-]/;
        }
        //if (opts.masks.hasOwnProperty(format)) { format = opts.masks[format]; }
        if (date && date != null) {
            if (!isNaN(date - 0) && String(format).toLowerCase() === "u") {
                //Unix timestamp
                timestamp = new Date(parseFloat(date) * 1000);
            } else if (date.constructor === Date) {
                timestamp = date;
                // Microsoft date format support
            } else if (msMatch !== null) {
                timestamp = new Date(parseInt(msMatch[1], 10));
                if (msMatch[3]) {
                    var offset = Number(msMatch[5]) * 60 + Number(msMatch[6]);
                    offset *= ((msMatch[4] === '-') ? 1 : -1);
                    offset -= timestamp.getTimezoneOffset();
                    timestamp.setTime(Number(Number(timestamp) + (offset * 60 * 1000)));
                }
            } else {
                var offset = 0;
                //Support ISO8601Long that have Z at the end to indicate UTC timezone
                if (opts.srcformat === 'ISO8601Long' && date.charAt(date.length - 1) === 'Z') {
                    offset -= (new Date()).getTimezoneOffset();
                }
                date = String(date).replace(/\T/g, "#").replace(/\t/, "%").split(opts.parseRe);
                format = format.replace(/\T/g, "#").replace(/\t/, "%").split(opts.parseRe);
                // parsing for month names
                for (k = 0, hl = format.length; k < hl; k++) {
                    if (format[k] === 'M') {
                        dM = $.inArray(date[k], opts.monthNames);
                        if (dM !== -1 && dM < 12) { date[k] = dM + 1; ts.m = date[k]; }
                    }
                    if (format[k] === 'F') {
                        dM = $.inArray(date[k], opts.monthNames, 12);
                        if (dM !== -1 && dM > 11) { date[k] = dM + 1 - 12; ts.m = date[k]; }
                    }
                    if (format[k] === 'a') {
                        dM = $.inArray(date[k], opts.AmPm);
                        if (dM !== -1 && dM < 2 && date[k] === opts.AmPm[dM]) {
                            date[k] = dM;
                            ts.h = h12to24(date[k], ts.h);
                        }
                    }
                    if (format[k] === 'A') {
                        dM = $.inArray(date[k], opts.AmPm);
                        if (dM !== -1 && dM > 1 && date[k] === opts.AmPm[dM]) {
                            date[k] = dM - 2;
                            ts.h = h12to24(date[k], ts.h);
                        }
                    }
                    if (format[k] === 'g') {
                        ts.h = parseInt(date[k], 10);
                    }
                    if (date[k] !== undefined) {
                        ts[format[k].toLowerCase()] = parseInt(date[k], 10);
                    }
                }
                if (ts.f) { ts.m = ts.f; }
                if (ts.m === 0 && ts.y === 0 && ts.d === 0) {
                    return "&#160;";
                }
                ts.m = parseInt(ts.m, 10) - 1;
                var ty = ts.y;
                if (ty >= 70 && ty <= 99) { ts.y = 1900 + ts.y; }
                else if (ty >= 0 && ty <= 69) { ts.y = 2000 + ts.y; }
                timestamp = new Date(ts.y, ts.m, ts.d, ts.h, ts.i, ts.s, ts.u);
                //Apply offset to show date as local time.
                if (offset > 0) {
                    timestamp.setTime(Number(Number(timestamp) + (offset * 60 * 1000)));
                }
            }
        } else {
            timestamp = new Date(ts.y, ts.m, ts.d, ts.h, ts.i, ts.s, ts.u);
        }
        if (newformat === undefined) {
            return timestamp;
        }
        if (opts.masks.hasOwnProperty(newformat)) {
            newformat = opts.masks[newformat];
        }
        else if (!newformat) {
            newformat = 'Y-m-d';
        }
        var 
			G = timestamp.getHours(),
			i = timestamp.getMinutes(),
			j = timestamp.getDate(),
			n = timestamp.getMonth() + 1,
			o = timestamp.getTimezoneOffset(),
			s = timestamp.getSeconds(),
			u = timestamp.getMilliseconds(),
			w = timestamp.getDay(),
			Y = timestamp.getFullYear(),
			N = (w + 6) % 7 + 1,
			z = (new Date(Y, n - 1, j) - new Date(Y, 0, 1)) / 86400000,
			flags = {
			    // Day
			    d: pad(j),
			    D: opts.dayNames[w],
			    j: j,
			    l: opts.dayNames[w + 7],
			    N: N,
			    S: opts.S(j),
			    //j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th',
			    w: w,
			    z: z,
			    // Week
			    W: N < 5 ? Math.floor((z + N - 1) / 7) + 1 : Math.floor((z + N - 1) / 7) || ((new Date(Y - 1, 0, 1).getDay() + 6) % 7 < 4 ? 53 : 52),
			    // Month
			    F: opts.monthNames[n - 1 + 12],
			    m: pad(n),
			    M: opts.monthNames[n - 1],
			    n: n,
			    t: '?',
			    // Year
			    L: '?',
			    o: '?',
			    Y: Y,
			    y: String(Y).substring(2),
			    // Time
			    a: G < 12 ? opts.AmPm[0] : opts.AmPm[1],
			    A: G < 12 ? opts.AmPm[2] : opts.AmPm[3],
			    B: '?',
			    g: G % 12 || 12,
			    G: G,
			    h: pad(G % 12 || 12),
			    H: pad(G),
			    i: pad(i),
			    s: pad(s),
			    u: u,
			    // Timezone
			    e: '?',
			    I: '?',
			    O: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
			    P: '?',
			    T: (String(timestamp).match(timezone) || [""]).pop().replace(timezoneClip, ""),
			    Z: '?',
			    // Full Date/Time
			    c: '?',
			    r: '?',
			    U: Math.floor(timestamp / 1000)
			};
        return newformat.replace(token, function ($0) {
            return flags.hasOwnProperty($0) ? flags[$0] : $0.substring(1);
        });
    };

    $.sa.formatter = {
        integer: { thousandsSeparator: ",", defaultValue: '0' },
        number: { decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00' },
        currency: { decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "", suffix: "", defaultValue: '0.00' },
        date: {
            dayNames: [
                "日", "一", "二", "三", "四", "五", "六",
                "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六",
            ],
            monthNames: [
                "一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二",
                "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
            ],
            AmPm: ["am", "pm", "上午", "下午"],
            S: function (j) { return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th'; },
            srcformat: 'Y-m-d',
            newformat: 'Y-m-d',
            parseRe: /[#%\\\/:_;.,\t\s-]/,
            masks: {
                // see http://php.net/manual/en/function.date.php for PHP format used in jqGrid
                // and see http://docs.jquery.com/UI/Datepicker/formatDate
                // and https://github.com/jquery/globalize#dates for alternative formats used frequently
                // one can find on https://github.com/jquery/globalize/tree/master/lib/cultures many
                // information about date, time, numbers and currency formats used in different countries
                // one should just convert the information in PHP format
                ISO8601Long: "Y-m-d H:i:s",
                ISO8601Short: "Y-m-d",
                // short date:
                //    n - Numeric representation of a month, without leading zeros
                //    j - Day of the month without leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                // example: 3/1/2012 which means 1 March 2012
                ShortDate: "n/j/Y", // in jQuery UI Datepicker: "M/d/yyyy"
                // long date:
                //    l - A full textual representation of the day of the week
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                LongDate: "l, F d, Y", // in jQuery UI Datepicker: "dddd, MMMM dd, yyyy"
                // long date with long time:
                //    l - A full textual representation of the day of the week
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    s - Seconds, with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                FullDateTime: "l, F d, Y g:i:s A", // in jQuery UI Datepicker: "dddd, MMMM dd, yyyy h:mm:ss tt"
                // month day:
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                MonthDay: "F d", // in jQuery UI Datepicker: "MMMM dd"
                // short time (without seconds)
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                ShortTime: "g:i A", // in jQuery UI Datepicker: "h:mm tt"
                // long time (with seconds)
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    s - Seconds, with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                LongTime: "g:i:s A", // in jQuery UI Datepicker: "h:mm:ss tt"
                SortableDateTime: "Y-m-d\\TH:i:s",
                UniversalSortableDateTime: "Y-m-d H:i:sO",
                // month with year
                //    Y - A full numeric representation of a year, 4 digits
                //    F - A full textual representation of a month
                YearMonth: "F, Y" // in jQuery UI Datepicker: "MMMM, yyyy"
            },
            reformatAfterEdit: false,
            userLocalTime: false
        }
    };
    /*************************************************************************
    * DESC:跳转页面时用post方式传递参数 
    * SampleCode: $.sa.linkToPage('/ORG/ORG010',{code:'a01',name:'testname'})
    *             $.sa.linkToPage('/ORG/ORG010',{code:'a01',name:'testname'},{target:'_blank'})
    * CREATOR: jin.chunhe
    * CREATE-DATE:2014-10-17
    *************************************************************************/
    $.sa.linkToPage = function (url, param, option) {
        param = param || {};
        option = option || {};
        if (option.target) { option.target = '_selrf' }
        //parameter form
        var $link_param_form = $("#form_sa_link_param");
        if ($link_param_form.size() === 0) {
            $link_param_form = $('<form>', {
                id: 'form_sa_link_param'
            }).appendTo("body");
        }
        $link_param_form.attr({ method: 'post', action: url, target: option.target });

        for (var key in param) {
            $('<input>', { type: 'hidden', name: key, value: param[key] }).appendTo($link_param_form);
        }
        $link_param_form.submit().remove();
    };
    String.prototype.format = function (args) {
        var result = this;
        if (arguments.length > 0) {
            if (arguments.length == 1 && typeof (args) == "object") {
                for (var key in args) {
                    if (args[key] != undefined) {
                        var reg = new RegExp("({" + key + "})", "g");
                        result = result.replace(reg, args[key]);
                    }
                }
            }
            else {
                for (var i = 0; i < arguments.length; i++) {
                    if (arguments[i] != undefined) {
                        var reg = new RegExp("({)" + i + "(})", "g");
                        result = result.replace(reg, arguments[i]);
                    }
                }
            }
        }
        return result;
    }
})(jQuery);
