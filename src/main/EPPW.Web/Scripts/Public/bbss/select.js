﻿/// <reference path="../jquery-1.11.1.js" />



//单选框
$.fn.extend({
    valueField: function (valuefield) {
        var obj = $(this);
        if (arguments.length)
            obj.attr("valuefield", valuefield);
        else
            return obj.attr("valuefield");
    }
    , textField: function (textfield) {
        var obj = $(this);
        if (arguments.length)
            obj.attr("textfield", textfield);
        else
            return obj.attr("textfield");
    }
    , selectedItem: function (item) {
        var obj = $(this);
        var valuefield = obj.valueField();
        var textfield = obj.textField();
        if (arguments.length) {
            var selectedtextEL = obj.find(".selected-text");
            var beforeItem = obj.selectedItem();
            obj.find(".item-list .select-item").removeClass("selected");
            if (item) {
                var targetliEL = obj.find(".item-list .select-item[valuefield-" + valuefield + "='" + item[valuefield] + "']");
                targetliEL.addClass("selected");
                selectedtextEL.text(targetliEL.text());
                if (beforeItem[valuefield] != item[valuefield]) {
                    obj.trigger("selectedchange", { beforeItem: beforeItem, currentItem: item });
                }
            }
            else {
                selectedtextEL.text("");
            }
            // obj.blur();
        }
        else {
            var seletedLi = obj.find(".item-list .selected");
            item = new Object();
            item[valuefield] = seletedLi.attr("valuefield-" + valuefield);
            item[textfield] = seletedLi.attr("textfield-" + textfield);
            return item;
        }
    }
    , selectedValue: function (value) {
        var obj = $(this);
        var valuefield = obj.valueField();
        var textfield = obj.textField();
        if (arguments.length) {
            var targetliEL = obj.find(".item-list .select-item[valuefield-" + valuefield + "='" + value + "']");
            var item = new Object();
            item[valuefield] = targetliEL.attr("valuefield-" + valuefield);
            item[textfield] = targetliEL.attr("textfield-" + textfield);
            obj.selectedItem(item);
        }
        else {
            var seletedLi = obj.find(".item-list .selected");
            return seletedLi.attr("valuefield-" + valuefield);

        }
    }
    , initSelect: function () {
        var obj = $(this);
        obj.children().remove();
        var selectedTextEL = $('<button  class=" form-control selected-text  text-left dropdown-toggle" data-toggle="dropdown"></button>');
        var itemlistEL = $('<ul class="item-list list-group dropdown-menu" ></ul>');

        obj.append(selectedTextEL);
        obj.append(itemlistEL);
    }
    , insertItem: function (item, index) {
        var obj = $(this);
        var valuefield = obj.valueField();
        var textfield = obj.textField();
        if (!obj.has(".item-list")) {
            obj.append('<ul class="item-list list-group  dropdown-menu" ></li></ul>');
        }
        var itemlistEL = obj.find(".item-list");
        var itemEL = $('<li class="select-item"><a  href="#">' + item[textfield] + '</a></li>');
        itemEL.attr("valuefield-" + valuefield, item[valuefield]);
        itemEL.attr("textfield-" + textfield, item[textfield]);
        itemEL.on("click", function () {
            obj.selectedItem(item);
        });

        if (arguments.length > 1) {
            itemlistEL.find(".select-item").eq(index).after(itemEL);
        } else {
            itemlistEL.append(itemEL);
        }
    }

    //items： 绑定的数据
    //options :{promptType:下拉框第一行}
    , bindItems: function (items, options) {
        var obj = $(this);
        obj.initSelect();
        if (options) {
            var promptitem;
            switch (options.promptType) {
                case "ALL":
                    promptitem = {};
                    promptitem[obj.valueField()] = "%";
                    promptitem[obj.textField()] = "全部";
                    break;
                case "SELECT":
                    promptitem = {};
                    promptitem[obj.valueField()] = "";
                    promptitem[obj.textField()] = "请选择";
                    break;
            }
            if (promptitem) {
                obj.insertItem(promptitem);
                obj.selectedItem(promptitem);
            }
        }

        for (var index in items) {
            obj.insertItem(items[index]);
        }

    }

});

$(document).ready(function () {

    $(document).on("click.bs.dropdown", ".select .select-filter", function (event) {
        event.stopPropagation();
    });
});