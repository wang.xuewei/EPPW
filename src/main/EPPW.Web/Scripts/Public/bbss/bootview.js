
/*!
* ???????? jQuery Plugin v1.0
*
* Copyright 2014, E-Land beijing
*
* History
* v1.0 – 创建, kuang.yongsheng(2012=4/10/14)
*/

/// <reference path="../jquery-1.11.1.js" />


; (function ($, window, document, undefined) {
    var pluginName = 'bootview',
        defaults = {
            showbootviewindex:true,
            schemas:[],
            objvaluesettor:{},//bootview 中各对象的设置值和获取值得方法,比如:{ShopName:{get:function(obj){return obj.val()},set:function(obj,value,rowdata){(obj.val(value))}}}
            changeevent:"",
            getrowdata:null,//function(row){return {};}
            addrow:null,//function(rowdata){}
            collapserow:false,//绑定list 的时候是否隐藏行(只显示header)
            afterrowcreated:null
        };

     BootView =function(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    };

    // initialize function
    BootView.prototype.init = function () {
        var $this = $(this.element);
        var  bootview =this;

        //值变更事件
        var allevents = "change select.change shoparea.change "+bootview.options.changeevent;

        $this.on(allevents,"[fieldname]",function(){
            var currentrow = $(this).parents(".bootview-row:first");
            var oldstatus = bootview.getrowstatus(currentrow);
            if(oldstatus=="R"){
                bootview.setrowstatus(currentrow,"U");//将查询出来的行设置为修改状态
            }
            bootview.setrowheader(bootview.getrowdata(currentrow),currentrow);//设置header中绑定的对应的值一起变化
        });

        //header点击时，body 的显示与否
        $this.on("click",".panel-heading",function(){
           var row = $(this).parent(); 
           var body = $(".panel-body",row);      
           if(body.is(".hidden"))
           {
                bootview.expandrow(row);
           }else{
                bootview.collapserow(row);
           }
        });
        $this.on("bootview.rowcreated",function(e,args){
            if(bootview.options.afterrowcreated){
                bootview.options.afterrowcreated(e,args);
            }
        });
    };

    // public function (use - test.data("pluginName").foo())
    //BootView.prototype.foo = function () {
    //    var $this = $(this.element);
    //};

    BootView.prototype.createrowobj = function(rowdata,schema,status){ 
        if(!schema) {
            schema =  this. options.schemas[0];
        }
        else if(schema.constructor==Number){
            schema = this. options.schemas[schema];
        }  
        var newrow = schema.clone(true).removeClass("bootview-schema").addClass("bootview-row");
        if(!status)
        {
            status="C";//CRUD
        }
        $(this.element).trigger("bootview.rowcreated",{row:newrow,rowdata:rowdata,schema:schema,status:status});
        this.setrowstatus(newrow,status);
        this.setrowdata(rowdata,newrow);
        this.setrowheader(rowdata,newrow);
        return newrow;
    };

    BootView.prototype.addrow = function(rowdata,schema,status){      
        var $this = $(this.element);
        if(this.options.addrow){
            this.options.addrow(rowdata);
        }
        else{
           var newrow= this.createrowobj(rowdata,schema,"C");
           $this.append(newrow);
           this.initbootviewindex(this.getrowindex(newrow));
        }
    };
    BootView.prototype.deleterow = function(rows){      
        var $this = $(this.element);
        var deleterowindex = this.getrowindex(rows)
        rows.remove();
        this.initbootviewindex(deleterowindex);
    };
    BootView.prototype.clear = function(){
        var $this= $(this.element);
         $(".bootview-row",$this).remove();
    }
    BootView.prototype.bind = function(list,getschema){
      var  $this= $(this.element);
        this.clear();
        for(var index in list)
        {
            var rowdata = list[index];
            var schema;
            if(getschema){
                schema = getschema(rowdata);
            }
            var newrow = this.createrowobj(rowdata,schema,"R");
            if( this.options.collapserow){
                this.collapserow(newrow);
            }
            $this.append(newrow);
        }
        this.initbootviewindex(0);
    };


    //获取对应index 的行 (从0开始)
    BootView.prototype.getrow = function(rowindex){
      return  $(".bootview-row:eq("+rowindex+")",this.element) ;    
    };

    //获取当前对象所对应的行
    BootView.prototype.getcurrentrow = function(obj){
        return $(obj).parents(".bootview-row").first();
    };

    BootView.prototype.expandrow = function(row){
        var header = $(".panel-heading",row);
        var body = $(".panel-body",row);
         
        $(".bootview-toolbar .panel-body-collapseflag",header).remove();
          $(".bootview-toolbar", header).append('<span class="panel-body-collapseflag glyphicon glyphicon-chevron-up"></span>');
        body.removeClass("hidden");
       
    };
    BootView.prototype.collapserow = function(row){
        var header = $(".panel-heading",row);
        var body = $(".panel-body",row);  
        $(".bootview-toolbar .panel-body-collapseflag",header).remove();
         $(".bootview-toolbar", header).append('<span class="panel-body-collapseflag glyphicon glyphicon-chevron-down"></span>');
        body.addClass("hidden"); 
    };

    BootView.prototype.initbootviewindex=function(startindex){
        var bootview =this;
        var $this = $(this.element);
        
        var initbootviewrow ;
        if(!startindex){
            initbootviewrow=$(".bootview-row",$this);
        }
        else{
            initbootviewrow = $(".bootview-row:gt("+(startindex-1)+")",$this);
        }
        initbootviewrow.each(function(index){
            bootview.setobjvalue($("[headername='BootViewIndex']",this),startindex+index+1);
        });
    }; 

    BootView.prototype.getrowdata=function(row){
         var  bootview =this;
         var data = {};
         $("[fieldname]",row).each(function(){
            var obj = $(this);
            data[obj.attr("fieldname")]=  bootview.getobjvalue(obj);
         });
        return data;                      
    };

    BootView.prototype.setrowdata=function(rowdata,row){
        for(var fieldname in rowdata){
            var propertyvalue = rowdata[fieldname];
            var propertyobj = $("[fieldname='"+fieldname+"']",row);
            this.setobjvalue(propertyobj,propertyvalue,rowdata);
        }                 
    };
    BootView.prototype.setrowheader=function(rowdata,row){
        for(var headername in rowdata){
            var propertyvalue = rowdata[headername];
            var propertyobj = $("[headername='"+headername+"']",row);
            this.setobjvalue(propertyobj,propertyvalue);
        }                 
    };

     BootView.prototype.getrowindex=function(row){
        var $this = $(this.element);
        return $(".bootview-row",$this).index(row)
        
     }; 
     BootView.prototype.getrowstatus=function(row){
        return row.attr("status");
     };   
     BootView.prototype.setrowstatus=function(row,status){
        row.attr("status",status);
     }; 

      BootView.prototype.getchangedrowsdata = function(){
         var bootview = this;
         var changedrowsdata = [];
         $(".bootview-row[status='U']",this.element).each(function(){
          changedrowsdata.push(bootview.getrowdata($(this)));
         });   
         return changedrowsdata;
    };

     BootView.prototype.getinsertrowsdata = function(){
        var bootview = this;
        var changedrowsdata = [];
        $(".bootview-row[status='C']",this.element).each(function(){
        changedrowsdata.push(bootview.getrowdata($(this)));
        });   
        return changedrowsdata; 
    };
  


     BootView.prototype.getobjvalue=function(obj){
        var bootview = this;
        var value;
        var fieldname =obj.attr("fieldname");
        if(bootview.options.objvaluesettor[fieldname]){
            value = bootview.options.objvaluesettor[fieldname].get(obj);
        }
        else if(obj.is("input")){
            var objtype= obj.attr("type");
            switch(objtype){
                case "checkbox":  
                    value=obj.prop("checked"); 
                break;
                default:
                   value= obj.val();
                break;
            }
        }
        else if(obj.is(".select")){
            value = obj.dropdownlist().selectedValue()
        }else if(obj.is("img")){
            value=obj.attr("value");
        }
        else if(obj.is("textarea"))
        {
            value=obj.val();
        }
       
        else{
            value=obj.html();  
        }
        return value;
    };

 
    BootView.prototype.setobjvalue=function(obj,value,rowdata){
        var $this = $(this.element);
        var  bootview =this;
        var fieldname =obj.attr("fieldname");
        if(bootview.options.objvaluesettor[fieldname]){
            bootview.options.objvaluesettor[fieldname].set(obj,value,rowdata);
        }
        else if(obj.is("input")){
            var objtype= obj.attr("type");
            switch(objtype){
                case "checkbox":
                    if(value)
                        obj.prop("checked",value); 
                break;
                default:
                    obj.val(value);
                break;
            }
            if(obj.is(".bootview-date")){
                obj.datetimepicker("update");
            }
        } 
        else if(obj.is(".select")){
            obj.dropdownlist().selectedValue(value)
        } 
        else if(obj.is("img")){
            var oldvalue = bootview.getobjvalue(obj);
            if(oldvalue!=value){
                obj.attr("src","../../file/getfile/"+value);
                obj.attr("value",value);
                obj.trigger("change")
            }
        }
        else if(obj.is("textarea"))
        {
            obj.val(value);
        }
     
        else{
            obj.html(value);  
        }
    };
   
    // create jQuery plugin
    $.fn[pluginName] = function (options) {
      this.each(function () {
            if (!$.data(this, pluginName)) {
                $.data(this, pluginName, new BootView(this, options));
            }
        });
        return  this.data(pluginName);
    }
//    $.fn[pluginName]  = function(options){
//        return  new BootView(this, options);
//    }

})(jQuery, window, document);
