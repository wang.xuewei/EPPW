﻿/// <reference path="../jquery-1.11.1.js" />
/// <reference path="jqueryextend.js" />
/// <reference path="../../../Content/public/bootstrap3/bootstrap.css" />
/// <reference path="../../../Content/public/EPPW/validate.css" />
/// <reference path="dropdownlist.js" />



$.fn.extend({
    valForValidate: function () {
        var obj = $(this);
        if (obj.is("input,textarea,select")) {
            return obj.val();
        }
        else if (obj.is(".select")) {
            return obj.dropdownlist().selectedValue(); ;
        }
        else if (obj.is("td")) {
            var value = obj.text();
            if (obj.has("input").length > 0)
                value = $("input", obj).val();

            return value.replace(/\&nbsp;/g, "");
        }
        else if (obj.is("img")) {
            return obj.attr("value");
        }
        else {
            return obj.text();
        }

    }
    , validateFail: function (options) {
        //options 为开发者提供提示的内容 
        //type:验证的类型
        var obj = $(this);
        var errorType = obj.attr("errorType");
        if (!errorType) {
            errorType = options.type + ",";
        } else {
            errorType = errorType.replace(options.type + ",", "")
            errorType += options.type + ",";
        }
        obj.attr("errorType", errorType);
    }
    , validateSuccess: function (options) {
        //options 为开发者提供提示的内容 
        //type:验证的类型
        var obj = $(this);
        var errorType = obj.attr("errorType");
        if (errorType) {
            errorType = errorType.replace(options.type + ",", "")
            obj.attr("errorType", errorType);
        }
    }
    , clearValidate: function () {
        var obj = $(this);
        obj.attr("errorType", "");
        obj.tooltip("destroy");
        //        obj.parent().removeClass("has-feedback");
        obj.parent().removeClass("has-error");
        obj.parent().removeClass("has-warning");
        obj.parent().removeClass("has-success");
        obj.removeClass("error-object");
        obj.removeClass("bg-danger");


        var icon = obj.siblings(".form-control-feedback");
        if (icon.length > 0) {
            icon.remove();
        }

    }
    , totalValidate: function () {
        //该元素内部的所有设置了 class=validate 全部进行验证 
        var validates = $(this).find(".validate");

        validates.each(function () {
            var obj = $(this);
            obj.trigger("blur");


        });
        var errors = $(this).find(".validate.error-object");
        if (errors.length > 0) {
            var firstErrorEL = errors.first();
            firstErrorEL.focus();
            var errorType = firstErrorEL.attr("errorType");
            var finalErrorType = null;
            if (errorType && errorType != "") {
                types = errorType.split(",");
                finalErrorType = types[types.length - 2]
            }

            if (finalErrorType) {
                var message = firstErrorEL.attr("validate-" + finalErrorType + "-message");
                var examples = firstErrorEL.attr("validate-" + finalErrorType + "-examples");
                var title = "<div class='text-left'>" + message + "</div>";

                if (examples) {

                    title += "<div class='text-left validate-example'>";
                    title += "<label class=''>" + $.getResource("Example") + ":</label></br>";

                    title += examples + "</br>";

                    title += "</div>";
                }
                $.sa.warning(title);
            }

            return false;
        } else {
            return true;
        }
    }
});



//非空验证
$(document).ready(function () {
    $(document).on("blur change", ".validate.notnull", function () {
        var obj = $(this);
        var value = obj.valForValidate();

        if ($.trim(value) == "") {
            var message = obj.attr("validate-notnull-message");
            var examples = obj.attr("validate-notnull-examples");
            if (!message)
                obj.attr("validate-notnull-message", $.getResource("WRN0019"));
            //if (!examples)
            //     obj.attr("validate-notnull-examples", "");
            obj.validateFail({ type: "notnull" });
        }
        else {
            obj.validateSuccess({ type: "notnull" });
        }
    });
});

$(document).ready(function () {
    //整数字的验证
    $(document).on("blur change", ".validate.int", function () {
        var obj = $(this);
        var int = obj.valForValidate();
        var maxintlength = obj.attr("validate-maxintlength")



        var regint = new RegExp("^\\d{0," + maxintlength + "}$");
        if (!regint.test(int)) {
            var message = obj.attr("validate-int-message");
            var examples = obj.attr("validate-int-examples");
            if (!message)
                obj.attr("validate-int-message", $.getResource("WRN0013").format(maxintlength));
            if (!examples)
                obj.attr("validate-int-examples", $.sa.randomInt(0, Math.pow(10, maxintlength)));

            obj.validateFail({ type: "int" });
        }
        else {

            obj.validateSuccess({ type: "int" });

        }
    });
});




//千位号的验证

$.fn.extend({
    valueofComma: function () {
        return $.sa.commaToNum($(this).valForValidate());
    }
});
$(document).ready(function () {
    $(document).on("blur change", ".validate.comma", function () {
        var obj = $(this);
        var comma = obj.valueofComma();
        var maxintlength = obj.attr("validate-maxintlength")
        if (!maxintlength)
            maxintlength = 15;
        var maxdecimallength = obj.attr("validate-maxdecimallength")
        if (!maxdecimallength)
            maxdecimallength = 2;
        var regComma = new RegExp("^[0-9\.\,]{0,}$");
         var regDecimal;
        if (maxdecimallength === "0") {
            regDecimal = new RegExp("^\\d{0," + maxintlength + "}$");
        }
        else {
            regDecimal = new RegExp("^(\\d{1," + maxintlength + "}(\\.\\d{1," + maxdecimallength + "}){0,1}){0,1}$");
        }
       
        if (!regDecimal.test(comma) || !regComma.test(obj.valForValidate())) {
            var message = obj.attr("validate-comma-message");
            var examples = obj.attr("validate-comma-examples");
            if (!message)
                obj.attr("validate-comma-message", $.getResource("WRN0014").format(maxintlength, maxdecimallength));
            if (!examples) {
                var exampleint = $.sa.randomInt(0, Math.pow(10, maxintlength));
                var exampledecimal = $.sa.randomInt(0, Math.pow(10, maxdecimallength)) / Math.pow(10, maxdecimallength)
                var example = exampleint + exampledecimal;

                obj.attr("validate-comma-examples", $.sa.numToComma(example, maxdecimallength));
            }
            obj.validateFail({ type: "comma" });
        }
        else {

            obj.validateSuccess({ type: "comma" });
            obj.val($.sa.numToComma(comma, maxdecimallength));
        }
    });
    $(document).on("focus", ".validate.comma", function () {
        var obj = $(this);
        obj.val(obj.valueofComma());

    });
});



//百分号验证 
$.fn.extend({
    valuefPercent: function () {
        return parseFloat($.trim($(this).val()).replace(/\,/g, "").replace("%", "")) / 100;
    }
});
///....






//最大值验证 
$(document).ready(function () {
    $(document).on("blur change", ".validate.max", function () {
        var obj = $(this);
        var maxvalue = parseInt($(this).attr("validate-maxvalue"));

        if (parseFloat(obj.valueofComma()) > maxvalue) {
            var message = obj.attr("validate-max-message");
            var examples = obj.attr("validate-max-examples");
            if (!message)
                obj.attr("validate-max-message", $.getResource("WRN0015").format(maxvalue));
            if (!examples)
                obj.attr("validate-max-examples", Math.floor($.sa.randomNumber(0, maxvalue)));
            obj.validateFail({ type: "max" });
        }
        else {
            obj.validateSuccess({ type: "max" });
        }
    });
});

//最小值验证 
$(document).ready(function () {
    $(document).on("blur change", ".validate.min", function () {
        var obj = $(this);
        var minvalue = parseInt($(this).attr("validate-minvalue"));

        if (parseFloat($.sa.commaToNum(obj.valForValidate())) < minvalue) {
            var message = obj.attr("validate-min-message");
            if (!message)
                obj.attr("validate-min-message", $.getResource("WRN0016").format(minvalue));
            obj.validateFail({ type: "min" });
        }
        else {
            obj.validateSuccess({ type: "min" });
        }
    });
});

//长度验证
$(document).ready(function () {
    $(document).on("blur change", ".validate.maxlength", function () {
        var obj = $(this);
        var maxlength = parseInt($(this).attr("validate-maxlength"));

        if (obj.valForValidate().length > maxlength) {
            var message = obj.attr("validate-maxlength-message");
            var examples = obj.attr("validate-maxlength-examples");
            if (!message)
                obj.attr("validate-maxlength-message", $.getResource("WRN0017").format(maxlength));

            obj.validateFail({ type: "maxlength" });
        }
        else {
            obj.validateSuccess({ type: "maxlength" });
        }
    });
});

//code验证
$(document).ready(function () {
    $(document).on("blur change", ".validate.code", function () {
        var obj = $(this);
        var regcode = new RegExp("^[0-9a-zA-Z]{0,}$");
        if (!regcode.test(obj.valForValidate())) {
            var message = obj.attr("validate-code-message");
            var examples = obj.attr("validate-code-examples");
            if (!message)
                obj.attr("validate-code-message", $.getResource("WRN0018"));

            obj.validateFail({ type: "code" });
        }
        else {
            obj.validateSuccess({ type: "code" });
        }
    });
});


//email验证
$(document).ready(function () {
    $(document).on("blur change", ".validate.email", function () {
        var obj = $(this);
        var regemail = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
        if (!regemail.test(obj.valForValidate())) {
            var message = obj.attr("validate-email-message");
            var examples = obj.attr("validate-email-examples");
            if (!message)
                obj.attr("validate-email-message", $.getResource("WRN0022"));

            obj.validateFail({ type: "email" });
        }
        else {
            obj.validateSuccess({ type: "email" });
        }
    });
});

//登录名验证
$(document).ready(function () {
    $(document).on("blur change", ".validate.username", function () {
        var obj = $(this);
        var regusername = /^[a-z][\.a-z0-9_]*[a-zA-Z0-9]$/;
        if (!regusername.test(obj.valForValidate())) {
            var message = obj.attr("validate-username-message");
            var examples = obj.attr("validate-username-examples");
            if (!message)
                obj.attr("validate-username-message", $.getResource("WRN0023"));

            obj.validateFail({ type: "username" });
        }
        else {
            obj.validateSuccess({ type: "username" });
        }
    });
});

//验证输入的密码和确认密码是否相同
$(document).ready(function () {
    $(document).on("blur change", ".validate.passwordcomfirm", function () {
        var obj = $(this);

        if (obj.valForValidate() != $(obj.attr("target")).valForValidate()) {
            var message = obj.attr("validate-passwordcomfirm-message");
            var examples = obj.attr("validate-passwordcomfirm-examples");
            if (!message)
                obj.attr("validate-passwordcomfirm-message", $.getResource("WRN0024"));

            obj.validateFail({ type: "passwordcomfirm" });
        }
        else {
            obj.validateSuccess({ type: "passwordcomfirm" });
        }
    });
});

//手机号验证，1开头，11位，电话号验证，区号+号码，区号3到4位，号码7到8位，可以有-链接也可以没有。
$(document).ready(function () {
    $(document).on("blur change", ".validate.telphone", function () {
        var obj = $(this);
        if (obj.valForValidate() == null || $.trim(obj.valForValidate()) == "") {
            obj.validateSuccess({ type: "telphone" });
            return;
        }
        var regtelMobile = /^1\d{10}$/;
        var regphone = /^0\d{2,3}-?\d{7,8}$/;
        if (!regtelMobile.test(obj.valForValidate()) && !regphone.test(obj.valForValidate())) {
            var message = obj.attr("validate-telphone-message");
            var examples = obj.attr("validate-telphone-examples");
            if (!message)
                obj.attr("validate-telphone-message", $.getResource("WRN0025"));

            obj.validateFail({ type: "telphone" });
        }
        else {
            obj.validateSuccess({ type: "telphone" });
        }
    });
});


$(document).ready(function () {

    $(document).on("select.change", ".select.validate.notnull", function () {
        var obj = $(this);
        var value = obj.valForValidate();

        if ($.trim(value) == "") {
            var message = obj.attr("validate-notnull-message");
            var examples = obj.attr("validate-notnull-examples");
            if (!message)
                obj.attr("validate-notnull-message", $.getResource("WRN0019"));
            //if (!examples)
            //     obj.attr("validate-notnull-examples", "");
            obj.validateFail({ type: "notnull" });
        }
        else {
            http: //localhost:12910/sample/select#
            obj.validateSuccess({ type: "notnull" });
        }
    });
});



//最终确认error的类型，并显示或者消除error
$(document).ready(function () {
    var fn_finalValidate = function (obj) {

        var errorType = obj.attr("errorType");
        var finalErrorType = null;
        if (errorType && errorType != "") {
            types = errorType.split(",");
            finalErrorType = types[types.length - 2]
        }

        if (finalErrorType) {
            obj.parent().addClass("has-error");
            //obj.parent().addClass("has-feedback");
            obj.addClass("error-object");

            obj.parent().removeClass("has-success");
            obj.parent().removeClass("has-warning");
            if (obj.is(".validate-table")) {
                obj.addClass("bg-danger");
            }
            else {
                //                var icon = obj.siblings(".form-control-feedback");
                //                if (icon.length > 0) {
                //                    icon.removeClass();
                //                    icon.addClass("glyphicon form-control-feedback");
                //                    icon.addClass("glyphicon-remove");
                //                }
                //                else {
                //                    obj.after('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
                //                }
                var message = obj.attr("validate-" + finalErrorType + "-message");
                var examples = obj.attr("validate-" + finalErrorType + "-examples");
                var title = "<div class='text-left'>" + message + "</div>";
                if (examples) {

                    title += "<div class='text-left validate-example'>";
                    title += "<label class=''>例如:</label></br>"
                    title += examples + "</br>";
                    title += "</div>";
                }
                obj.tooltip("destroy");
                obj.tooltip({
                    animation: false
                        , trigger: "hover"
                        , html: true
                        , placement: "top"
                        , template: '<div class="tooltip validate"  role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner "></div></div>'
                        , title: title
                        , container: 'body'
                });
            }


        } else {
            obj.clearValidate();
            // 添加validate-showsuccess 类后，会在验证成功后有所标识
            if (obj.is(".validate-showsuccess")) {
                //                obj.parent().addClass("has-feedback");
                obj.parent().addClass("has-success");
                //                var icon = obj.siblings(".form-control-feedback");
                //                if (icon.length > 0) {
                //                    icon.removeClass();
                //                    icon.addClass("glyphicon form-control-feedback");
                //                    icon.addClass("glyphicon-ok");
                //                }
                //                else {
                //                    obj.parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
                //                }
                obj.tooltip("destroy");
            }
        }
    };

    $(document).on("blur change", ".validate", function () {
        var obj = $(this);
        fn_finalValidate(obj);
    });

    $(document).on("select.change", ".select.validate", function () {
        var obj = $(this);
        fn_finalValidate(obj);
    });

});