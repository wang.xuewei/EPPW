﻿
/*!
* ???????? jQuery Plugin v1.0
*
* Copyright 2014, E-Land beijing
*
* History
* v1.0 – 创建, kuang.yongsheng(2012=4/10/22)
*/

/// <reference path="../jquery-1.11.1.js" />


; (function ($, window, document, undefined) {
    var pluginName = 'dropdownlist',
        defaults = {
            textfield:"Name",//显示的内容
            valuefield:"Value",//实际的值
            items:[],//待绑定的数据源
            defaultvalue:null,//默认的值
            disabled:false,
            prompttype:"",//下拉框 第一行的 类型 （all/select）
            selectinit:null,//初始化结束事件
            selectchange:null//选择值变更事件
        };

    DropDownList = function (element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);
        dropdownlist =this;
        dropdownlist._defaults = defaults;
        dropdownlist._name = pluginName;

        var $this= $(dropdownlist.element);
        $this.on("select.init",function(e,data){
            dropdownlist.options.initcompleted =true;
            if(dropdownlist.options.selectinit){
                dropdownlist.options.selectinit(e,data);
            }
        });
        $this.on("select.change",function(e,data){
             if(dropdownlist.options.selectchange){
                dropdownlist.options.selectchange(e,data);
            }
        });


        dropdownlist.init();
        

    };

    // initialize function
    DropDownList.prototype.init = function (options) {
        var $this = $(this.element);
        var dropdownlist = this;
        
        dropdownlist.bind();
        dropdownlist.disabled(dropdownlist.options.disabled);
    };

    //绑定数据源
    DropDownList.prototype.bind=function(items,defaultvalue){
        var $this = $(this.element);
        var dropdownlist = this;
        if(items){
            dropdownlist.options.items = items;
        }
        if(defaultvalue){
            dropdownlist.options.defaultvalue = defaultvalue;
        }
        $this.children().remove();
        var selectedTextEL = $('<button  class=" form-control selected-text  text-left dropdown-toggle" data-toggle="dropdown"></button>');
        var itemlistEL = $('<ul class="item-list list-group dropdown-menu" ></ul>');
        $this.append(selectedTextEL);
        $this.append(itemlistEL);
        var promptitem;
        if(dropdownlist.options.prompttype){
            switch (dropdownlist.options.prompttype) {
                case "all":
                    promptitem = {};
                    promptitem[dropdownlist.options.valuefield] = "%";
                    promptitem[dropdownlist.options.textfield] = $.getResource("All");
                    break;
                case "select":
                    promptitem = {};
                    promptitem[dropdownlist.options.valuefield] = "";
                    promptitem[dropdownlist.options.textfield] = $.getResource("Select");
                    break;
            }
            if (promptitem) {
                dropdownlist.insertItem(promptitem);
            }
        }
        for(var index in dropdownlist.options.items)
        {
            dropdownlist.insertItem(dropdownlist.options.items[index]);
        }
        if(dropdownlist.options.defaultvalue !==null){
            dropdownlist.selectedValue(dropdownlist.options.defaultvalue);
        }else if(promptitem){
            dropdownlist.selectedItem(promptitem);
        }
    }
    // public function (use - test.data("pluginName").foo())
    //DropDownList.prototype.foo = function () {
    //    var $this = $(this.element);
    
    DropDownList.prototype.selectedItem = function (item){
        var $this = $(this.element);
        var dropdownlist = this;
        var valuefield = dropdownlist.options.valuefield;
        var textfield = dropdownlist.options.textfield;
        if (arguments.length) {
            var selectedtextEL = $this.find(".selected-text");
            var beforeItem = dropdownlist.selectedItem();
            $this.find(".item-list .select-item").removeClass("selected");
            if (item) {
                var targetliEL = $this.find(".item-list .select-item[valuefield-" + valuefield + "='" + item[valuefield] + "']");
                targetliEL.addClass("selected");
                selectedtextEL.text(targetliEL.text());
                if (beforeItem[valuefield] != item[valuefield] &&  dropdownlist.options.initcompleted) {
                    $this.trigger("select.change", { beforeItem: beforeItem, currentItem: item });
                }
            }
            else {
                selectedtextEL.text("");
            }
            
        }
        else {
            var seletedLi = $this.find(".item-list .selected");
            item = new Object();
            item[valuefield] = seletedLi.attr("valuefield-" + valuefield);
            item[textfield] = seletedLi.attr("textfield-" + textfield);
            return item;
        }
    };

     DropDownList.prototype.selectedValue = function (value){
        var $this = $(this.element);
        var dropdownlist = this;
        var valuefield = dropdownlist.options.valuefield;
        var textfield = dropdownlist.options.textfield;
        if (arguments.length) {
            var targetliEL = $this.find(".item-list .select-item[valuefield-" + valuefield + "='" + value + "']");
            var item = new Object();
            item[valuefield] = targetliEL.attr("valuefield-" + valuefield);
            item[textfield] = targetliEL.attr("textfield-" + textfield);
            dropdownlist.selectedItem(item);
        }
        else {
            var seletedLi = $this.find(".item-list .selected");
            return seletedLi.attr("valuefield-" + valuefield);

        }
    }
    DropDownList.prototype.disabled = function(disabled){
        var $this = $(this.element);
        if(disabled){
           $(".selected-text",$this).attr("disabled","disabled")
        }else{
             $(".selected-text",$this).removeAttr("disabled")
        }
        this.options.disabled =disabled;
     };
    DropDownList.prototype.insertItem = function (item){
        var $this = $(this.element);
        var dropdownlist = this;
        var valuefield = dropdownlist.options.valuefield;
        var textfield = dropdownlist.options.textfield;
        if (!$this.has(".item-list")) {
            $this.append('<ul class="item-list list-group  dropdown-menu" ></li></ul>');
        }
        var itemlistEL = $this.find(".item-list");
        var itemEL = $('<li class="select-item"><a>' + item[textfield] + '</a></li>');
        itemEL.attr("valuefield-" + valuefield, item[valuefield]);
        itemEL.attr("textfield-" + textfield, item[textfield]);
        itemEL.on("click", function () {
            dropdownlist.selectedItem(item);
        });

        if (arguments.length > 1) {
            itemlistEL.find(".select-item").eq(index).after(itemEL);
        } else {
            itemlistEL.append(itemEL);
        }

    };

    // create jQuery plugin
    $.fn[pluginName] = function (options) {
        if(options){
            this.each(function (){
                    $(this).data(pluginName, new DropDownList(this, options));
                    $(this).trigger("select.init",{dropdownlist:$(this).data(pluginName)});
            });
            
        }
        return this.data(pluginName);
    }

})(jQuery, window, document);
