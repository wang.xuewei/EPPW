﻿/// <reference path="../jquery-1.11.1.js" />

//培训教程的处理

var trainingScripts;
var currentTrainingScript;
$(document).ready(function () {
    var programCode = route_Action;
    $("#liTrainingScripts").on("click", function () {
        if (!trainingScripts) {
            $.sa.get("../Common/GetTrainingScriptsByProramCode"
                , {
                    programCode: programCode
                }
                , function (data) {
                    trainingScripts = data.TrainingScripts;
                    fn_ShowTrainingScripts(trainingScripts);
                }
            );
        }
        else{
            fn_ShowTrainingScripts(trainingScripts);
        } 
    });

    var fn_ShowTrainingScripts = function (trainingScripts) {
        if (trainingScripts.length == 0) {
            $.sa.warning($.getResource("WRN0012"), {
                maxWidth: 300,
                title: $.getResource("TrainingCourse")
            });
            return;
        }
        var content = $('<ul style="margin:-10px"></ul>');
        for (var index in trainingScripts) {
            var script = trainingScripts[index];
            var liScript = $('<li ><a href="#" title="' + script.Message + '">' + script.Caption + '</a></li>')
            $("a", liScript).data("script", script);
            content.append(liScript);
        }
        var scriptsPop = $.sa.confirm(content, {
            maxWidth: 300,
       
            showOKBtn: false,
            showCancelBtn: false,
            showCloseBtn: false,
            title: $.getResource("TrainingCourse")
        });

        content.on("click", "a", function () {
            var obj = $(this);
            var script = obj.data("script");
            scriptsPop.close();
            fn_StartTrainingScript(script);
        });
    };



    var fn_ClearTrainingScript = function () {
        if (currentTrainingScript && currentTrainingScript.Steps) {
            for (var index in currentTrainingScript.Steps) {
                $(currentTrainingScript.Steps[index].selector).popover("destroy");
            }
        }
    }

    var fn_StartTrainingScript = function (script) {

        fn_ClearTrainingScript();
        currentTrainingScript = script;

        fn_ShowScriptStep(0, currentTrainingScript);
    }

    var fn_ShowScriptStep = function (index, script) {
        var lastStep = script.Steps[index - 1];
        if (lastStep) {
            $(lastStep.Selector).popover("destroy");
        }
        if (index == script.Steps.length) {
            return;
        }
        var currentStep = script.Steps[index];
        var currentEL = $(currentStep.Selector);


        var popoverTitle = currentStep.Title;

        var trainingContent = $('<div>' + currentStep.Content + '</div>')
        var trainingFooter = $('<div class="training-footer"></div>');
        var trainingOperator = $('<div class="pull-right btn-toolbar"></div>');

        var trainingOperator_end = $('<button class="btn btn-default  btn-sm">' + $.getResource("End") + '</button>').on("click", function () {
            currentEL.popover("destroy");
        });
        var trainingOperator_next = $('<button class="btn btn-default btn-sm">' + $.getResource("Next") + '</button>').on("click", function () {
            fn_ShowScriptStep(index + 1, script);
        });

        trainingOperator.append(trainingOperator_end);
        if (index < script.Steps.length - 1)//最有一步不需要“下一步”按钮
            trainingOperator.append(trainingOperator_next);
        trainingFooter.append(trainingOperator);

        var popoverContent = $("<div><div/>").append(trainingContent).append(trainingFooter);
        var trainingoptions = {
            container: 'body'
                , content: popoverContent
                , placement: currentStep.Placement ? currentStep.Placement : "bottom"
                , title: popoverTitle
                , trigger: "manual"
                , html: true
        };

        currentEL.popover(trainingoptions);
        currentEL.popover("show");
        currentEL.focus();
    }
});