﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPPW.Collections
{
    [Serializable]
    public class NameValueList : List<NameValueObject>
    {
        public IEnumerable<string> NameList
        {
            get
            {
                List<string> nameList = new List<string>();
                if (base.Count != 0)
                {
                    foreach (NameValueObject nameValueObject in this)
                    {
                        nameList.Add(nameValueObject.Name);
                    }
                }
                return nameList;
            }
        }

        public void Add(string name, object value)
        {
            if (this.NameList.Contains(name) == true)
            {
                throw new ArgumentException();
            }
            base.Add(new NameValueObject(name, value));
        }

        public void Remove(string name)
        {
            NameValueObject target = null;
            foreach (NameValueObject nameValueObject in this)
            {
                if (nameValueObject.Name == name)
                {
                    target = nameValueObject;
                    break;
                }
            }

            if (target == null) throw new KeyNotFoundException();
            base.Remove(target);
        }

        public bool ContainsName(string name)
        {
            foreach (NameValueObject nameValueObject in this)
            {
                if (nameValueObject.Name == name)
                {
                    return true;
                }
            }
            return false;
        }

        public object this[string name]
        {
            get
            {
                foreach (NameValueObject nameValueObject in this)
                {
                    if (nameValueObject.Name == name) return nameValueObject.Value;
                }
                throw new KeyNotFoundException();
            }
            set
            {
                foreach (NameValueObject nameValueObject in this)
                {
                    if (nameValueObject.Name == name)
                    {
                        nameValueObject.Value = value;
                        return;
                    }
                }
                throw new KeyNotFoundException();
            }
        }
    }

    [Serializable]
    public class NameValueObject
    {
        public string Name { get; set; }
        public object Value { get; set; }

        public NameValueObject() { }
        public NameValueObject(string name, object value)
        {
            this.Name = name;
            this.Value = value;
        }
    }
}
