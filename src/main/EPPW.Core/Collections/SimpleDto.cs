﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPPW.Collections
{
    /// <summary>
    /// for bind dropdownlist
    /// </summary>
    [Serializable]
    public class SimpleDto
    {
        public string Code { get; set; }
        public string Desc { get; set; }
        public string Param { get; set; }

    }
}
