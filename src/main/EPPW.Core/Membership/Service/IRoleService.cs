﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;

namespace EPPW.Membership.Service
{
    public interface IRoleService
    {
        List<RoleDto> GetAllRoleDto();
        RoleDto GetRoleDto(int id);
        void AddRoleDto(RoleDto roleDto);
        void UpdateRoleDto(RoleDto roleDto);
        void DeleteRoleDtoById(int id);
        List<ProgramDto> GetProgramDtoDtoByRoleId(int id);
        List<UserDto> GetUserDtoDtoByRoleId(int id);
        void AddUserDtoInRole(int roleId,int userId);
        void AddProgramDtoInRole(int roleId, int programId);
        void DeleteUserDtoInRole(int roleId, int userId);
        void DeleteProgramDtoInRole(int roleId, int programId);
        List<ProgramDto> GetAllActionOfHasAccessRightUser(int userId);

    }
}
