﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;

namespace EPPW.Membership.Service
{
    public interface IUserService
    {
        void SantInit();
        List<UserDto> GetAllUserDto();
        UserDto GetUserDto(int id);
        void AddUserDto(UserDto userDto);
        void UpdateUserDto(UserDto userDto);
        void DeleteUserDtoById(int id);
        List<RoleDto> GetRoleDtoByUserId(int userId);
        UserDto GetUserByUserName(string userName);
    }
}
