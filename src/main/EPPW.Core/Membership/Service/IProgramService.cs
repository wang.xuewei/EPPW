﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;

namespace EPPW.Membership.Service
{
    public interface IProgramService
    {
        List<ProgramDto> GetAllProgramDto();
        ProgramDto GetProgramDtoById(int id);
        void AddProgramDto(ProgramDto programDto,int userId);
        void UpdateProgramDto(ProgramDto programDto, int userId);
        void DeleteProgramDtoById(int id);
        List<RoleDto> GetRoleDtoByProgramId(int id);

    }
}
