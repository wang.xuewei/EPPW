﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;

namespace EPPW.Membership.Service
{
    public interface IProgramMenuService
    {
        List<ProgramMenuDto> GetAllProgramMenuDto();
        ProgramMenuDto GetProgramMenuDto(int id);
        void AddProgramMenuDto(ProgramMenuDto programMenuDto);
        void UpdateProgramMenuDto(ProgramMenuDto programMenuDto);
        void DeleteProgramMenuDtoById(int id);
        void UpdateProgramIdByMenuId(int menuId, int? programId);
        List<ProgramMenuDto> GetProgramMenuByUserId(int userId);
    }
}
