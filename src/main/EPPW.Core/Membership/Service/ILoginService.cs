﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;

namespace EPPW.Membership.Service
{
    public interface ILoginService
    {
        UserDto Login(string userName, string password);
    }
}
