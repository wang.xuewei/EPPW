﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPPW.Membership.Dto
{
     [Serializable]
    public class ProgramMenuDto
    {
        public string ApplicationId { get { return "E23756DA-E5DB-4AF0-9167-A1D401746F31"; }  }
        public int DisplaySeq { get; set; }
        public int? ParentId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Parameter { get; set; }
        public int? ProgramId { get; set; }

        public string ProgramName { get; set; }//add by xiao.xinmiao
        public string Controller { get; set; }//add by xiao.xinmiao
        public string Action { get; set; }//add by xiao.xinmiao

    }
}
