﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPPW.Membership.Dto
{
    [Serializable]
    public class ProgramDto
    {
        //以下属性对应的表sant.Programs
        public string ApplicationId { get { return "E23756DA-E5DB-4AF0-9167-A1D401746F31"; }  }
        public int ProgramId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsUsed { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int Creator { get; set; }
        public int Modifier { get; set; }
        public int RoleCnt { get; set; }


        // 以下属性对应的表sant.ProgramForWeb
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Area { get; set; }
        public string IconName { get; set; }
        public string Parameter { get; set; }


        public DtoStatus Status { get; set; }//Add By JiangYafei 2015-1-6 15:36:31

    }
}
