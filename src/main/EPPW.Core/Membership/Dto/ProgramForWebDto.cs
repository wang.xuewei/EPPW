﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPPW.Membership.Dto
{
    [Serializable]
    public class ProgramForWebDto
    {
        public  int Id { get; set; }
        public  string Action { get; set; }
        public  string Controller { get; set; }
        public  string Area { get; set; }
        public  string IconName { get; set; }
        public  string Parameter { get; set; }
    }
}
