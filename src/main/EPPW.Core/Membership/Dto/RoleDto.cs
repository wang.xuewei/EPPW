﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPPW.Membership.Dto
{
    [Serializable]
    public class RoleDto
    {
        public string ApplicationId { get { return "E23756DA-E5DB-4AF0-9167-A1D401746F31"; } }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public bool IsUsed { get; set; }
        public int UserCnt { get; set; }
        public int ProgramCnt { get; set; }
    }
}
