﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPPW.FileMng.Dto
{
    [Serializable]
    public class FileDto
    {
        public int FileID { get; set; }
   
        public string FileName { get; set; }
        public string FileExtName { get; set; }
        public string FilePath { get; set; }
        public bool UseChk { get; set; }
        public bool DelChk { get; set; }
        public int InUserID { get; set; }
        public string InUserName { get; set; }
        public DateTime? InDateTime { get; set; }
   
     
    }
}
