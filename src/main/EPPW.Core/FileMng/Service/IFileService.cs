﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.FileMng.Dto;
using EPPW.Collections;

namespace EPPW.FileMng.Service
{
    public interface IFileService
    {
        FileDto GetFileByFileID(NameValueList args);
        FileDto SaveFile(NameValueList args);
    }
}
