﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Collections;

namespace EPPW.SampleMng.Service
{
    public interface ISampleService
    {
        DateTime GetDate();
        void GetSqlException();
    }

}
