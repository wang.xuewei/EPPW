﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Collections;
using EPPW.HomeMng.Dao;

namespace EPPW.HomeMng.Service
{
    public class HomeService : IHomeService
    {
        public IHomeDao HomeDao { private get; set; }


        public void SaveInvest(NameValueList param)
        {
            HomeDao.SaveInvest(param);
        }
    }
}
