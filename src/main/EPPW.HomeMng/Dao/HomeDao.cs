﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Data.NHibernate.Generic.Support;
using EPPW.Collections;

namespace EPPW.HomeMng.Dao
{
    public class HomeDao : HibernateDaoSupport, IHomeDao
    {
        public void SaveInvest(NameValueList param)
        {
            Session.GetNamedQuery("HomeDao_SaveInvest")
               .SetParameter("Tel", param["Tel"])
               .SetParameter("Email", param["Email"])
               .SetParameter("Attendance", param["Chk1"])
               .SetParameter("Sale", param["Chk2"])
               .SetParameter("Delivery", param["Chk3"])
               .SetParameter("Inventory", param["Chk4"])
               .SetParameter("Stock", param["Chk5"])
               .SetParameter("Finance", param["Chk6"])
               .SetParameter("Report", param["Chk7"])
               .SetParameter("Remark", param["Remark"])
               .List();
        }
    }
}
