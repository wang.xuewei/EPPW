﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sliverant.Core.Security;
using EPPW.Membership.Dto;
using EPPW.Membership.Dao;
using Sliverant.Core.Exceptions;

namespace EPPW.Membership.Service
{
    public class LoginService:ILoginService
    {
        public ILoginDao LoginDao { private get; set; }


        public UserDto Login(string userName, string password)
        {
            UserDto userDto = LoginDao.Login(userName, password.Encrypt());
            if(userDto==null)
                throw new LoginFailedException("ErrorAccountOrPassword");
            return userDto;
        }
    }
}
