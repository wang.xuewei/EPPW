﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;
using EPPW.Membership.Dao;

namespace EPPW.Membership.Service
{
    public class ProgramService : IProgramService
    {
        public IProgramDao ProgramDao { private get; set; }

        public void AddProgramDto(ProgramDto programDto, int userId)
        {
            ProgramDao.AddProgramDto(programDto, userId);
        }

        public void DeleteProgramDtoById(int id)
        {
            ProgramDao.DeleteProgramDtoById(id);
        }

        public List<ProgramDto> GetAllProgramDto()
        {
            return ProgramDao.GetAllProgramDto();
        }

        public ProgramDto GetProgramDtoById(int id)
        {
            return ProgramDao.GetProgramDtoById(id);
        }

        public void UpdateProgramDto(ProgramDto programDto, int userId)
        {
            ProgramDao.UpdateProgramDto(programDto, userId);
        }


        public List<RoleDto> GetRoleDtoByProgramId(int id)
        {
            return ProgramDao.GetRoleDtoByProgramId(id);
        }
    }
}
