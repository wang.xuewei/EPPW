﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;
using Sliverant.Core.Security;
using EPPW.Membership.Dao;

namespace EPPW.Membership.Service
{
    public class UserService : IUserService
    {
        public IUserDao UserDao { private get; set; }
        public void SantInit()
        {
            UserDto userDto = new UserDto();
            userDto.IsAnonymous = false;
            userDto.UserName = "admin";
            userDto.Email = "admin@admin.com";

            userDto.Name = "管理员";
            userDto.Password = "1234".Encrypt();
            userDto.PasswordQuestion = "Question";
            userDto.PasswordAnswer = "Answer";
            userDto.Comment = "Comment";

            this.AddUserDto(userDto);
        }
        public List<UserDto> GetAllUserDto()
        {
            return UserDao.GetAllUserDto();
        }
        public UserDto GetUserDto(int id)
        {
            return UserDao.GetUserDto(id);

        }
        public void UpdateUserDto(UserDto userDto)
        {
            UserDao.UpdateUserDto(userDto);

        }
        public void AddUserDto(UserDto userDto)
        {
            UserDao.AddUserDto(userDto);
        }
        public void DeleteUserDtoById(int id)
        {
            UserDao.DeleteUserDtoById(id);
        }


        public List<RoleDto> GetRoleDtoByUserId(int userId)
        {
            return UserDao.GetRoleDtoByUserId(userId);
        }


        public UserDto GetUserByUserName(string userName)
        {
            return UserDao.GetUserByUserName(userName);
        }
    }
}
