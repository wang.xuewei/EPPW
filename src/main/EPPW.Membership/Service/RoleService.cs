﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;
using EPPW.Membership.Dao;

namespace EPPW.Membership.Service
{
    public class RoleService : IRoleService
    {
        public IRoleDao RoleDao { private get; set; }

        public List<RoleDto> GetAllRoleDto()
        {
            return RoleDao.GetAllRoleDto();
        }
        public RoleDto GetRoleDto(int id)
        {
            return RoleDao.GetRoleDto(id);
        }
        public void AddRoleDto(RoleDto roleDto)
        {
            RoleDao.AddRoleDto(roleDto);
        }
        public void DeleteRoleDtoById(int id)
        {
            RoleDao.DeleteRoleDtoById(id);
        }
        public void UpdateRoleDto(RoleDto roleDto)
        {
            RoleDao.UpdateRoleDto(roleDto);
        }


        public List<ProgramDto> GetProgramDtoDtoByRoleId(int id)
        {
            return RoleDao.GetProgramDtoDtoByRoleId(id);
        }

        public List<UserDto> GetUserDtoDtoByRoleId(int id)
        {
            return RoleDao.GetUserDtoDtoByRoleId(id);

        }

        public void AddProgramDtoInRole(int roleId, int userId)
        {
            RoleDao.AddProgramDtoInRole(roleId,userId);
        }

        public void AddUserDtoInRole(int roleId, int programId)
        {
            RoleDao.AddUserDtoInRole(roleId, programId);
        }
        public void DeleteUserDtoInRole(int roleId, int userId)
        {
            RoleDao.DeleteUserDtoInRole(roleId, userId);
        }

        public void DeleteProgramDtoInRole(int roleId, int programId)
        {
            RoleDao.DeleteProgramDtoInRole(roleId, programId);
        }

        public List<ProgramDto> GetAllActionOfHasAccessRightUser(int userId)
        {
            return RoleDao.GetAllActionOfHasAccessRightUser(userId);
        }
    }
}
