﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPPW.Membership.Dto;
using EPPW.Membership.Dao;

namespace EPPW.Membership.Service
{
    public class ProgramMenuService : IProgramMenuService
    {
        public IProgramMenuDao ProgramMenuDao { private get; set; }

        public List<ProgramMenuDto> GetAllProgramMenuDto()
        {

            List<ProgramMenuDto> programMenuDtoList = ProgramMenuDao.GetAllProgramMenuDto();
            if (programMenuDtoList != null)
            {
                foreach (ProgramMenuDto dto in programMenuDtoList)
                {
                    if (dto.ProgramId == null)
                        dto.ProgramId = 0;
                }
            }
            return programMenuDtoList;
        }
        public ProgramMenuDto GetProgramMenuDto(int id)
        {
            ProgramMenuDto dto = ProgramMenuDao.GetProgramMenuDto(id);
            if (dto.ProgramId == null)
                dto.ProgramId = 0;
            return dto;
        }
        public void AddProgramMenuDto(ProgramMenuDto programMenuDto)
        {
            ProgramMenuDao.AddProgramMenuDto(programMenuDto);
        }
        public void DeleteProgramMenuDtoById(int id)
        {
            ProgramMenuDao.DeleteProgramMenuDtoById(id);
        }
        public void UpdateProgramMenuDto(ProgramMenuDto programMenuDto)
        {
            ProgramMenuDao.UpdateProgramMenuDto(programMenuDto);
        }


        public void UpdateProgramIdByMenuId(int menuId, int? programId)
        {
            ProgramMenuDao.UpdateProgramIdByMenuId(menuId, programId);
        }

        public List<ProgramMenuDto> GetProgramMenuByUserId(int userId)
        {
            return ProgramMenuDao.GetProgramMenuByUserId(userId);
        }
    }
}
