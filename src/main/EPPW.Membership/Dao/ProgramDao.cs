﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Data.NHibernate.Generic.Support;
using EPPW.Membership.Dto;
using NHibernate.Transform;
using NHibernate;

namespace EPPW.Membership.Dao
{
    public interface IProgramDao
    {
        List<ProgramDto> GetAllProgramDto();
        ProgramDto GetProgramDtoById(int id);
        void AddProgramDto(ProgramDto programDto, int userId);
        void UpdateProgramDto(ProgramDto programDto, int userId);
        void DeleteProgramDtoById(int id);
        List<RoleDto> GetRoleDtoByProgramId(int id);
        
    }

    public class ProgramDao : HibernateDaoSupport,IProgramDao
    {
        public List<ProgramDto> GetAllProgramDto()
        {
            return Session.GetNamedQuery("ProgramDao_GetAllProgramDto")
                .SetResultTransformer(Transformers.AliasToBean(typeof(ProgramDto)))
                .List<ProgramDto>() as List<ProgramDto>;
        }

        public ProgramDto GetProgramDtoById(int id)
        {
            return Session.GetNamedQuery("ProgramDao_GetProgramDtoById")
                .SetParameter("ProgramId", id, NHibernateUtil.Int32)
                .SetResultTransformer(Transformers.AliasToBean(typeof(ProgramDto)))
                .UniqueResult<ProgramDto>();
        }

        public void AddProgramDto(ProgramDto programDto, int userId)
        {
            Session.GetNamedQuery("ProgramDao_AddProgramDto")
                .SetParameter("Name",programDto.Name,NHibernateUtil.String)
                .SetParameter("Description",programDto.Description,NHibernateUtil.String)
                .SetParameter("UserId", userId, NHibernateUtil.Int32)
                .SetParameter("Code",programDto.Code,NHibernateUtil.String)
                .SetParameter("Controller",programDto.Controller,NHibernateUtil.String)
                .SetParameter("Action",programDto.Action,NHibernateUtil.String)
                .SetParameter("Area",programDto.Area,NHibernateUtil.String)
                .SetParameter("IconName",programDto.IconName,NHibernateUtil.String)
                .SetParameter("Parameter",programDto.Parameter,NHibernateUtil.String)
                .List();
        }

        public void UpdateProgramDto(ProgramDto programDto, int userId)
        {
            Session.GetNamedQuery("ProgramDao_UpdateProgramDto")
             .SetParameter("ProgramId", programDto.ProgramId, NHibernateUtil.Int32)
             .SetParameter("Name", programDto.Name, NHibernateUtil.String)
             .SetParameter("Description", programDto.Description, NHibernateUtil.String)
             .SetParameter("UserId", userId, NHibernateUtil.Int32)
             .SetParameter("Code", programDto.Code, NHibernateUtil.String)
             .SetParameter("Controller", programDto.Controller, NHibernateUtil.String)
             .SetParameter("Action", programDto.Action, NHibernateUtil.String)
             .SetParameter("Area", programDto.Area, NHibernateUtil.String)
             .SetParameter("IconName", programDto.IconName, NHibernateUtil.String)
             .SetParameter("Parameter", programDto.Parameter, NHibernateUtil.String)
             .List();
        }
        public void DeleteProgramDtoById(int id)
        {
            Session.GetNamedQuery("ProgramDao_DeleteProgramDtoById")
                .SetParameter("ProgramId", id, NHibernateUtil.Int32)
                .List();
        }

        public List<RoleDto> GetRoleDtoByProgramId(int id)
        {
            return Session.GetNamedQuery("ProgramDao_GetRoleDtoByProgramId")
                 .SetParameter("Id", id, NHibernateUtil.Int32)
                 .SetResultTransformer(Transformers.AliasToBean(typeof(RoleDto)))
                 .List<RoleDto>() as List<RoleDto>;
        }

    }
}
