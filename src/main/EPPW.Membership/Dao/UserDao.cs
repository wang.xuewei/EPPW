﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Data.NHibernate.Generic.Support;
using EPPW.Membership.Dto;
using NHibernate.Transform;
using NHibernate;

namespace EPPW.Membership.Dao
{
    public interface IUserDao
    {
        List<UserDto> GetAllUserDto();
        UserDto GetUserDto(int id);
        void AddUserDto(UserDto userDto);
        void UpdateUserDto(UserDto userDto);
        void DeleteUserDtoById(int id);
        List<RoleDto> GetRoleDtoByUserId(int userId);
        UserDto GetUserByUserName(string userName);
    }

    public class UserDao : HibernateDaoSupport,IUserDao
    {


        public List<UserDto> GetAllUserDto()
        {
            return Session.GetNamedQuery("UserDao_GetAllUserDto")
                .SetResultTransformer(Transformers.AliasToBean(typeof(UserDto)))
                .List<UserDto>() as List<UserDto>;
        }

        public UserDto GetUserDto(int id)
        {
            return Session.GetNamedQuery("UserDao_GetUserDto")
                .SetParameter("Id", id, NHibernateUtil.Int32)
                .SetResultTransformer(Transformers.AliasToBean(typeof(UserDto)))
                .UniqueResult<UserDto>();
        }

        public void AddUserDto(UserDto userDto)
        {
            Session.GetNamedQuery("UserDao_AddUserDto")
            .SetParameter("UserName", userDto.UserName, NHibernateUtil.String)
            .SetParameter("Email", userDto.Email, NHibernateUtil.String)
            .SetParameter("Password", userDto.Password, NHibernateUtil.String)
            .SetParameter("Name", userDto.Name, NHibernateUtil.String)
            .SetParameter("PasswordQuestion", userDto.PasswordQuestion, NHibernateUtil.String)
            .SetParameter("PasswordAnswer", userDto.PasswordAnswer, NHibernateUtil.String)
            .SetParameter("Comment", userDto.Comment, NHibernateUtil.String)
            .List();
        }

        public void UpdateUserDto(UserDto userDto)
        {
            Session.GetNamedQuery("UserDao_UpdateUserDto")
             .SetParameter("UserId", userDto.UserId, NHibernateUtil.Int32)
             .SetParameter("UserName", userDto.UserName, NHibernateUtil.String)
             .SetParameter("Email", userDto.Email, NHibernateUtil.String)
             .SetParameter("Password", userDto.Password, NHibernateUtil.String)
             .SetParameter("Name", userDto.Name, NHibernateUtil.String)
             .SetParameter("PasswordQuestion", userDto.PasswordQuestion, NHibernateUtil.String)
             .SetParameter("PasswordAnswer", userDto.PasswordAnswer, NHibernateUtil.String)
             .SetParameter("Comment", userDto.Comment, NHibernateUtil.String)
             .List();
        }

        public void DeleteUserDtoById(int id)
        {
            Session.GetNamedQuery("UserDao_DeleteUserDtoById")
             .SetParameter("UserId", id, NHibernateUtil.Int32)
             .List();
        }


        public List<RoleDto> GetRoleDtoByUserId(int userId)
        {
            return Session.GetNamedQuery("UserDao_GetRoleDtoByUserId")
               .SetParameter("Id", userId, NHibernateUtil.Int32)
               .SetResultTransformer(Transformers.AliasToBean(typeof(RoleDto)))
               .List<RoleDto>() as List<RoleDto>;
        }


        public UserDto GetUserByUserName(string userName)
        {
            return Session.GetNamedQuery("UserDao_GetUserByUserName")
               .SetParameter("UserName", userName, NHibernateUtil.String)
               .SetResultTransformer(Transformers.AliasToBean(typeof(UserDto)))
               .UniqueResult<UserDto>();
        }
    }
}
