﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Data.NHibernate.Generic.Support;
using EPPW.Membership.Dto;
using NHibernate.Transform;
using NHibernate;

namespace EPPW.Membership.Dao
{
    public interface IProgramMenuDao
    {
        List<ProgramMenuDto> GetAllProgramMenuDto();
        ProgramMenuDto GetProgramMenuDto(int id);
        void AddProgramMenuDto(ProgramMenuDto programMenuDto);
        void UpdateProgramMenuDto(ProgramMenuDto programMenuDto);
        void DeleteProgramMenuDtoById(int id);
        void UpdateProgramIdByMenuId(int menuId, int? programId);
        List<ProgramMenuDto> GetProgramMenuByUserId(int userId);
    }

    public class ProgramMenuDao : HibernateDaoSupport, IProgramMenuDao
    {
        public List<ProgramMenuDto> GetAllProgramMenuDto()
        {
            return Session.GetNamedQuery("ProgramMenuDao_GetAllProgramMenuDto")
                 .SetResultTransformer(Transformers.AliasToBean(typeof(ProgramMenuDto)))
                 .List<ProgramMenuDto>() as List<ProgramMenuDto>;
        }

        public ProgramMenuDto GetProgramMenuDto(int id)
        {
            return Session.GetNamedQuery("ProgramMenuDao_GetProgramMenuDto")
                    .SetParameter("Id", id, NHibernateUtil.Int32)
                    .SetResultTransformer(Transformers.AliasToBean(typeof(ProgramMenuDto)))
                    .UniqueResult<ProgramMenuDto>();
        }

        public void AddProgramMenuDto(ProgramMenuDto programMenuDto)
        {
            Session.GetNamedQuery("ProgramMenuDao_AddProgramMenuDto")
                .SetParameter("ParentId", programMenuDto.ParentId, NHibernateUtil.Int32)
                .SetParameter("DisplaySeq", programMenuDto.DisplaySeq, NHibernateUtil.Int32)
                .SetParameter("Name", programMenuDto.Name, NHibernateUtil.String)
                .SetParameter("Description", programMenuDto.Description, NHibernateUtil.String)
                .SetParameter("Parameter", programMenuDto.Parameter, NHibernateUtil.String)
                .List();
        }

        public void UpdateProgramMenuDto(ProgramMenuDto programMenuDto)
        {
            Session.GetNamedQuery("ProgramMenuDao_UpdateProgramMenuDto")
                .SetParameter("Id", programMenuDto.Id, NHibernateUtil.Int32)
                .SetParameter("ParentId", programMenuDto.ParentId, NHibernateUtil.Int32)
                .SetParameter("DisplaySeq", programMenuDto.DisplaySeq, NHibernateUtil.Int32)
                .SetParameter("Name", programMenuDto.Name, NHibernateUtil.String)
                .SetParameter("Description", programMenuDto.Description, NHibernateUtil.String)
                .SetParameter("Parameter", programMenuDto.Parameter, NHibernateUtil.String)
                .List();
        }

        public void DeleteProgramMenuDtoById(int id)
        {
            Session.GetNamedQuery("ProgramMenuDao_DeleteProgramMenuDtoById")
                .SetParameter("Id", id, NHibernateUtil.Int32)
                .List();
        }

        public void UpdateProgramIdByMenuId(int menuId, int? programId)
        {
            Session.GetNamedQuery("ProgramMenuDao_UpdateProgramIdByMenuId")
             .SetParameter("Id", menuId, NHibernateUtil.Int32)
             .SetParameter("ProgramId", programId, NHibernateUtil.Int32)
             .List();
        }

        public List<ProgramMenuDto> GetProgramMenuByUserId(int userId)
        {
            return Session.GetNamedQuery("ProgramMenuDao_GetProgramMenuByUserId")
                  .SetParameter("UserId", userId, NHibernateUtil.Int32)
                  .SetResultTransformer(Transformers.AliasToBean(typeof(ProgramMenuDto)))
                  .List<ProgramMenuDto>() as List<ProgramMenuDto>;
        }
    }
}
