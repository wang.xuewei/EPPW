﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Data.NHibernate.Generic.Support;
using EPPW.Membership.Dto;
using NHibernate;
using NHibernate.Transform;

namespace EPPW.Membership.Dao
{
    public interface ILoginDao
    {
  
        UserDto Login(string userName, string password);
    }

    public class LoginDao : HibernateDaoSupport, ILoginDao
    {
        public UserDto Login(string userName, string password)
        {
            return Session.GetNamedQuery("LoginDao_Login")
                .SetParameter("UserName", userName, NHibernateUtil.String)
                .SetParameter("Password", password, NHibernateUtil.String)
                .SetResultTransformer(Transformers.AliasToBean(typeof(UserDto)))
                .UniqueResult<UserDto>();
        }
    }
}
