﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Data.NHibernate.Generic.Support;
using EPPW.Membership.Dto;
using NHibernate.Transform;
using NHibernate;

namespace EPPW.Membership.Dao
{
    public interface IRoleDao
    {
        List<RoleDto> GetAllRoleDto();
        RoleDto GetRoleDto(int id);
        void AddRoleDto(RoleDto roleDto);
        void UpdateRoleDto(RoleDto roleDto);
        void DeleteRoleDtoById(int id);
        List<ProgramDto> GetProgramDtoDtoByRoleId(int id);
        List<UserDto> GetUserDtoDtoByRoleId(int id);
        void AddUserDtoInRole(int roleId, int userId);
        void AddProgramDtoInRole(int roleId, int programId);
        void DeleteUserDtoInRole(int roleId, int userId);
        void DeleteProgramDtoInRole(int roleId, int programId);
        List<ProgramDto> GetAllActionOfHasAccessRightUser(int userId);
    }

    public class RoleDao : HibernateDaoSupport,IRoleDao
    {
        public List<RoleDto> GetAllRoleDto()
        {
            return Session.GetNamedQuery("RoleDao_GetAllRoleDto")
                     .SetResultTransformer(Transformers.AliasToBean(typeof(RoleDto)))
                     .List<RoleDto>() as List<RoleDto>;
        }

        public RoleDto GetRoleDto(int id)
        {
            return Session.GetNamedQuery("RoleDao_GetRoleDto")
                   .SetParameter("Id", id, NHibernateUtil.Int32)
                   .SetResultTransformer(Transformers.AliasToBean(typeof(RoleDto)))
                   .UniqueResult<RoleDto>();
        }

        public void AddRoleDto(RoleDto roleDto)
        {
            Session.GetNamedQuery("RoleDao_AddRoleDto")
            .SetParameter("RoleName", roleDto.RoleName, NHibernateUtil.String)
            .SetParameter("Description", roleDto.Description, NHibernateUtil.String)
            .List();
        }

        public void UpdateRoleDto(RoleDto roleDto)
        {
            Session.GetNamedQuery("RoleDao_UpdateRoleDto")
               .SetParameter("RoleId", roleDto.RoleId, NHibernateUtil.Int32)
               .SetParameter("RoleName", roleDto.RoleName, NHibernateUtil.String)
               .SetParameter("Description", roleDto.Description, NHibernateUtil.String)
               .List();
        }

        public void DeleteRoleDtoById(int id)
        {
            Session.GetNamedQuery("RoleDao_DeleteRoleDtoById")
            .SetParameter("RoleId", id, NHibernateUtil.Int32)
            .List();
        }


        public List<ProgramDto> GetProgramDtoDtoByRoleId(int id)
        {
            return Session.GetNamedQuery("RoleDao_GetProgramDtoDtoByRoleId")
             .SetParameter("Id", id, NHibernateUtil.Int32)
             .SetResultTransformer(Transformers.AliasToBean(typeof(ProgramDto)))
             .List<ProgramDto>() as List<ProgramDto>;
        }

        public List<UserDto> GetUserDtoDtoByRoleId(int id)
        {
            return Session.GetNamedQuery("RoleDao_GetUserDtoDtoByRoleId")
                .SetParameter("Id", id, NHibernateUtil.Int32)
                .SetResultTransformer(Transformers.AliasToBean(typeof(UserDto)))
                .List<UserDto>() as List<UserDto>;
        }


        public void AddUserDtoInRole(int roleId, int userId)
        {
            Session.GetNamedQuery("RoleDao_AddUserDtoInRole")
                .SetParameter("UserId", userId, NHibernateUtil.Int32)
                .SetParameter("RoleId", roleId, NHibernateUtil.Int32)
                .List();
        }

        public void AddProgramDtoInRole(int roleId, int programId)
        {
            Session.GetNamedQuery("RoleDao_AddProgramDtoInRole")
                 .SetParameter("ProgramId", programId, NHibernateUtil.Int32)
                 .SetParameter("RoleId", roleId, NHibernateUtil.Int32)
                 .List();
        }


        public void DeleteUserDtoInRole(int roleId, int userId)
        {
            Session.GetNamedQuery("RoleDao_DeleteUserDtoInRole")
          .SetParameter("UserId", userId, NHibernateUtil.Int32)
          .SetParameter("RoleId", roleId, NHibernateUtil.Int32)
          .List();
        }


        public void DeleteProgramDtoInRole(int roleId, int programId)
        {
            Session.GetNamedQuery("RoleDao_DeleteProgramDtoInRole")
             .SetParameter("ProgramId", programId, NHibernateUtil.Int32)
             .SetParameter("RoleId", roleId, NHibernateUtil.Int32)
             .List();
        }


        public List<ProgramDto> GetAllActionOfHasAccessRightUser(int userId)
        {
            return Session.GetNamedQuery("RoleDao_GetAllActionOfHasAccessRightUser")
               .SetParameter("UserId", userId, NHibernateUtil.Int32)
                .SetResultTransformer(Transformers.AliasToBean(typeof(ProgramDto)))
               .List<ProgramDto>() as List<ProgramDto>;
        }
    }
}
